<?php

namespace App\Form;

use App\Entity\ReponseClient;
use App\Entity\ReponseSolway;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ReponseClientType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    { $builder
        //->add('date')
        //->add('sender')
        //->add('receiver')
        //->add('ticket')
        ->add ('message',CKEditorType::class)
        
        //->add('ticket',EntityType::class,
        //[
            //'class' =>Ticket::class,
            //'choice_label' => 'id',
           
            
            
// ])
            
        ->add('envoyer',SubmitType::class)
    ;
}

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ReponseClient::class,
        ]);
    }
}
