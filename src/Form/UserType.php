<?php

namespace App\Form;

use App\Entity\Utilisateurs;
use Doctrine\DBAL\Types\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType as TypeTextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('email', EmailType::class,array('label'=>'Email'))
            //->add('roles')
            ->add('password',PasswordType::class,
            array('label'=>'mot de passe'))
    
            ->add('name',TypeTextType::class,array('label'=>'Nom de l\'utilisateur'))
            //->add('activation_token')
            //->add('reset_token')
            //->add('enabled')
          //  ->add('isVerified',null,array('label'=>'Activé'))
            ->add('roles',ChoiceType::class,[
                'multiple' => true,
                'expanded' => true, 
                
                'choices' =>[ 
                'administrateur'=>'ROLE_ADMIN',
                'Manager'=> "ROLE_MANAGER",
                'Client'=>"ROLE_USER"
                ]])
                ;

            

        
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Utilisateurs::class,
        ]);
    }
}
