<?php

namespace App\Form;

use App\Entity\CategorieTicket;
use App\Entity\DomainTicket;
use App\Entity\PrioriteTicket;
use App\Entity\RechercheTicket;
use App\Entity\Responsable;
use App\Entity\SousDomainTicket;
use App\Entity\StatutTicket;
use App\Entity\Utilisateurs;
use App\Repository\UtilisateursRepository;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RechercheTicketsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dateDebut', DateType::class, [
                'required' => false,
                'label' => 'Période du',
                'widget' => 'single_text',
                'attr' => ['class' => 'js-datepicker'],
                'label' => false,
            ])
            ->add('dateFin', DateType::class, [
                'required' => false,
                'label' => 'Au',
                'widget' => 'single_text',
                'attr' => ['class' => 'js-datepicker'],
                'label' => false,
            ])
            ->add('categorie', EntityType::class, [
                'required' => false,
                'class' => CategorieTicket::class,
                'choice_label' => 'nom_categorie',
                'placeholder' => 'rechercher par catégorie',
                'label' => false,
                'multiple' => false
            ])
            ->add('Domain', EntityType::class, [
                'required' => false,
                'class' => DomainTicket::class,
                'choice_label' => 'nom_domain',
                'placeholder' => 'rechercher par domaine',
                'label' => false,
                'multiple' => false
            ])
            ->add('sous_domain', EntityType::class, [
                'required' => false,
                'class' => SousDomainTicket::class,
                'placeholder' => 'rechercher par sous domain',
                'choice_label' => 'nom_ss_domain',
                'label' => false,
                'multiple' => false
            ])
            ->add('priorite', EntityType::class, [
                'required' => false,
                'class' => PrioriteTicket::class,
                'placeholder' => 'rechercher par priorité',
                'choice_label' => 'niveau_priorite',
                'label' => false,
                'multiple' => false
            ])
            ->add('status', EntityType::class, [
                'required' => false,
                'class' => StatutTicket::class,
                'placeholder' => 'rechercher par status',
                'choice_label' => 'etat_ticket',
                'label' => false,
                'multiple' => false
            ])
            ->add('Affectation', EntityType::class, [
                'class' => Utilisateurs::class,
                'query_builder' => function (UtilisateursRepository $er) {
                    return $er->createQueryBuilder('u')
                    ->orderBy('u.roles', 'ASC')
                   ->where('u.roles LIKE :role')
                    ->setParameter('role', '%"'.'ROLE_ADMIN'.'"%');
                    },
                'required' => false,
                'choice_label' => 'Email',
                'label' => false,
               'placeholder' => 'rechercher par client'
                ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => RechercheTicket::class,
        ]);
    }
}
