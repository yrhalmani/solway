<?php

namespace App\Form;

use App\Entity\CategorieTicket;
use App\Entity\DomainTicket;
use App\Entity\PrioriteTicket;
use App\Entity\Responsable;
use App\Entity\SousDomainTicket;
use App\Entity\Ticket;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use App\Entity\Utilisateurs;
use App\Repository\DomainTicketRepository;
use App\Repository\UtilisateursRepository;
use App\Repository\SousDomainTicketRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;

class AdminTicketType extends AbstractType
{
    public function __construct(private SousDomainTicketRepository $SousDomainTicketRepository){}
    
        
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titre_ticket', TextType::class, array(
                'label' => false,
                'attr' => array(
                    'placeholder' => 'Titre du ticket'
                )
           ))
          
            ->add('categorie', EntityType::class, [
                'required' => false,
                'class' => CategorieTicket::class,
                'choice_label' => 'nom_categorie',
                'multiple' => false,
                'label'=>false,
                'placeholder' => 'sélectionner une catégorie',
            ])
            /*->add('Domain', EntityType::class, [
                'required' => false,
                'class' => DomainTicket::class,
                'choice_label' => 'nom_domain',
                'multiple' => false,
                'label' => 'Domaine'
            ])
            
             ->add('sous_domain', EntityType::class, [
                'required' => false,
                'class' => SousDomainTicket::class,
                'choice_label' => 'nom_ss_domain',
                'multiple' => false,
                'label' => 'Sous domaine'
            ])

            */

         ->add('Priorite', EntityType::class, [
                'required' => false,
                'class' => PrioriteTicket::class,
                'choice_label' => 'niveau_priorite',
                'multiple' => false,
                'label' => false,
                 'placeholder' => 'sélectionner une priorité',
            ])
           

            ->add('utilisateurs', EntityType::class, [
                'required' => false,
                'class' => Utilisateurs::class,
                'query_builder' => function (UtilisateursRepository $er) {
                    return $er->createQueryBuilder('u')
                    ->orderBy('u.roles', 'ASC')
                   ->where('u.roles LIKE :role')
                    ->setParameter('role', '%"'.'ROLE_USER'.'"%');
                    },
                'choice_label' => 'email',
                'multiple' => false,
                'placeholder' => 'sélectionner un client',
                'label'=>false
            ])
         
     

            ->add('affectation', EntityType::class, [
                'class' => Utilisateurs::class,
                'query_builder' => function (UtilisateursRepository $er) {
                    return $er->createQueryBuilder('u')
                    ->orderBy('u.roles', 'ASC')
                   ->where('u.roles LIKE :role')
                   ->setParameter('role', '%"'.'ROLE_MANAGER'.'"%');
                    },

                'choice_label' => 'Email',
                //'label' => 'affeeectation',
                'placeholder' => 'Affectation',
                'label'=>false
            ])
            ->add('commentaire',CKEditorType::class)

            ->add('file', FileType::class, [
                 'label' => 'Fichier',
                 'multiple' => true,
                 'mapped' => false,
              'required' => false,
])

            ->add('Domain', EntityType::class, [
                'class' => DomainTicket::class,
                'choice_label' => 'nom_domain',
                'placeholder' => 'sélectionner un domaine',
                'label'=>false
                
            ])
            ->add('sous_domain',EntityType::class, [
                'required' => false,
                'class' => SousDomainTicket::class,
                'choice_label' => 'nom_ss_domain',
                'multiple' => false,
                'label'=>false,
                'placeholder' => 'sélectionner un sous domaine',
            ]) ; 
            }
                    
         
    

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Ticket::class,
        ]);
    }
}
