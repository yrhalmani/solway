<?php

namespace App\Form;

use App\Entity\Equipe;
use App\Entity\Utilisateurs;
use Dompdf\FrameDecorator\Text;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class UtilisateurType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('email',null,
            [
                'disabled' => $options['is_edit'],
                'invalid_message' =>'Email invalid',
                'label'=>'Email',
                'invalid_message' =>'Email nnon valid ',    
            ])
            ->add('name',null,
            [
                'disabled' => $options['is_edit'],
                'label'=>'Nom de l\'utilisateur'])


            ->add('roles',ChoiceType::class,[
                'multiple' => true,
                'expanded' => true, 
                
                'choices' =>[ 
                'administrateur'=>'ROLE_ADMIN',
                'Manager'=> "ROLE_MANAGER",
               // 'Utilisateur_solway' => 'ROLE_SOLWAY_USER'
                ]

                ])

               /* ->add('equipe',EntityType::class,[
                'class' =>Equipe::class,
                'choice_label' => 'nom_equipe',
                'label'=>'Equipe'

                ])*/

                ->add('submit',SubmitType::class,[
                    'attr'=> [
                    'class'=>'btn btn-success'
                    ]
                    ]);
            
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Utilisateurs::class,
            'is_edit' => false,
        ]);
    }
}
