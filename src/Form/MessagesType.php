<?php

namespace App\Form;

use App\Entity\Messages;
use App\Entity\Utilisateurs;
use Symfony\Component\Form\Extension\Core\Type\FileType;

use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MessagesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
       
        //->add('date')
        //->add('sender')
        //->add('receiver')
        //->add('ticket')
        ->add ('message',CKEditorType::class)
        
        //->add('ticket',EntityType::class,
        //[
            //'class' =>Ticket::class,
            //'choice_label' => 'id',
           
            
            
// ])
            
        
->add('file', FileType::class, [
    'label' => false,
    'multiple' => true,
    'mapped' => false,
    'required' => false,
])
        ->add('envoyer',SubmitType::class)
    ;
    
}

    

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Messages::class,
        ]);
    }
}