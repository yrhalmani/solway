<?php

namespace App\Form;

use App\Entity\DomainTicket;
use App\Entity\SousDomainTicket;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SousDomainesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('domain', EntityType::class, [
                'required' => false,
                'class' => DomainTicket::class,
                'choice_label' => 'nom_domain',
               'choice_label' => 'nom_domain',
                'label' => 'Domaine',
            ])
            ->add('nom_ss_domain',null,[
                'label' => 'Sous domaine']
                )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SousDomainTicket::class,
        ]);
    }
}
