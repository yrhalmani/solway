<?php

namespace App\Form;

use App\Entity\CategorieTicket;
use App\Entity\DomainTicket;
use App\Entity\PrioriteTicket;
use App\Entity\Responsable;
use App\Entity\SousDomainTicket;
use App\Entity\StatutTicket;
use App\Entity\Utilisateurs;
use App\Repository\UtilisateursRepository;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RechercheTypeAdmin extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {


        $builder
            ->add('dateDebut', DateType::class, [
                'required' => false,
                'label' => 'Du',
                'widget' => 'single_text',
                'attr' => ['class' => 'js-datepicker'],
            ])
            ->add('dateFin', DateType::class, [
                'required' => false,
                'label' => 'Au',
                'widget' => 'single_text',
                'attr' => ['class' => 'js-datepicker'],
            ])
            ->add('categorie', EntityType::class, [
                'required' => false,
                'class' => CategorieTicket::class,
                'choice_label' => 'nom_categorie',
                'placeholder' => 'rechercher par catégorie',
                'multiple' => false,
                'label' => false,
            ])
            ->add('Domain', EntityType::class, [
                'required' => false,
                'class' => DomainTicket::class,
                'choice_label' => 'nom_domain',
                'placeholder' => 'rechercher par domaine',
                'multiple' => false,
                'label' => false,
            ])
            ->add('sous_domain', EntityType::class, [
                'required' => false,
                'class' => SousDomainTicket::class,
                'placeholder' => 'rechercher par sous domaine',
                'choice_label' => 'nom_ss_domain',
                'multiple' => false,
                'label' => false,
            ])
            ->add('priorite', EntityType::class, [
                'required' => false,
                'class' => PrioriteTicket::class,
                'placeholder' => 'rechercher par priorité',
                'choice_label' => 'niveau_priorite',
                'multiple' => false,
                'label' => false,
            ])
            ->add('utilisateurs', EntityType::class, [
                'class' => Utilisateurs::class,
                'query_builder' => function (UtilisateursRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->orderBy('u.roles', 'ASC')
                        ->where('u.roles LIKE :role')
                        ->setParameter('role', '%"'.'ROLE_MANAGER'.'"%')
                        ->orWhere('u.roles LIKE :admin_role')
                        ->setParameter('admin_role', '%"'.'ROLE_ADMIN'.'"%');
                },
                'choice_label' => 'Email',
                'required' => false,
                'placeholder' => 'rechercher par Affectation',

                'label' => 'Affectation',

                'multiple' => false
            ])
            ->add('status', EntityType::class, [
                'required' => false,
                'class' => StatutTicket::class,
                'placeholder' => 'rechercher par status',
                'choice_label' => 'etat_ticket',
                'label' => false,
                'multiple' => false
            ])
             ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
