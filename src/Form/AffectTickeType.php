<?php

namespace App\Form;

use App\Entity\Ticket;
use App\Entity\CategorieTicket;
use App\Entity\DomainTicket;

use App\Entity\PrioriteTicket;
use App\Entity\Responsable;
use App\Entity\SousDomainTicket;
use App\Entity\Utilisateurs;
use App\Entity\StatutTicket;
use App\Repository\UtilisateursRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\ChoiceList\Factory\Cache\ChoiceLabel;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormEvent;
use FOS\CKEditorBundle\Form\Type\CKEditorType;

class AffectTickeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('id',null,['disabled'=>true])
            ->add('titre_ticket',null,['disabled'=>true,
            'label'=>'Titre du ticket',])
    
            
            ->add('categorie', EntityType::class, [
                'class'=> CategorieTicket::class,
                //'disabled' => $options['is_edit'],
                'choice_label' =>'nom_categorie',
                'label'=>'Catégorie',
                'placeholder' => 'sélectionner la catégorie',
                'disabled'=>true

            ])

            ->add('Priorite', EntityType::class, [
                'class' =>PrioriteTicket::class,
               // 'disabled' => $options['is_edit'],
                'choice_label' => 'niveau_priorite',
                'label'=>'Priorité',
                'disabled'=>true,
                

                'placeholder' => 'sélectionner la priorité de votre demande'
            ])

            ->add('Domain', EntityType::class, [
                //'mapped' => false,
                'class' => DomainTicket::class,
                //'disabled' => $options['is_edit'],
                'choice_label' => 'nom_domain',
                'placeholder' => 'selectionner le Domain',
                'label' => 'Domaine',
                'disabled'=>true
                 // 'required' => true
            ])

            ->add('sous_domain', EntityType::class, [
                'placeholder' => 'Domain (Choisir un sous domain)',
                'class' => SousDomainTicket::class,
                'disabled'=>true,
                'label'=>'Sous domaine',
                //'disabled' => $options['is_edit'],

                //'required' => false
            ])
         /*->add('responsable', EntityType::class, [
                'class' => Responsable::class,
                'choice_label' => 'nom_responsable',
                'placeholder' => 'attribuer un responsableee',
                'label' => 'responsable'


            ])*/



            ->add('statut_ticket',EntityType::class,[
                'class'=>StatutTicket::class,
                 'choice_label'=>'etat_ticket',
                 'disabled' => true,
                 'label'=>'Status',
            ])

            ->add('affectation', EntityType::class, [
                'class' => Utilisateurs::class,
                'query_builder' => function (UtilisateursRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->orderBy('u.roles', 'ASC')
                        ->where('u.roles LIKE :role')
                        ->setParameter('role', '%"'.'ROLE_MANAGER'.'"%')
                        ->orWhere('u.roles LIKE :admin_role')
                        ->setParameter('admin_role', '%"'.'ROLE_ADMIN'.'"%');
                },
                'choice_label' => 'Email',
                'label' => 'Affectation',
                'placeholder' => 'Affectation',
            ]) 
                 
                 
                 ->add('commentaire', CKEditorType::class,
                 [
                     //'disabled' => $options['is_edit'],
                     'disabled'=>true
                    
                 ]) 

            ->add('file', FileType::class, [
                'label' => false,
                'multiple' => true,
                'mapped' => false,
                'required' => false,
            ])
       
            

            ->add('Valider', SubmitType::class)
        ;
 

    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Ticket::class,
        ]);
    }
}
