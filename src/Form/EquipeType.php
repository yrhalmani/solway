<?php

namespace App\Form;

use App\Entity\Equipe;
use App\Entity\Utilisateurs;
use App\Repository\UtilisateursRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EquipeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('nom_equipe')
            ->add('manager', EntityType::class, [
                'class' => Utilisateurs::class,
                'query_builder' => function (UtilisateursRepository $er) {
                    return $er->createQueryBuilder('u')
                    ->orderBy('u.roles', 'ASC')
                   ->where('u.roles LIKE :role')
                    ->setParameter('role', '%"'.'ROLE_MANAGER'.'"%');
                    },
                'choice_label' => 'Email',
                'label' => 'Manager',
               'placeholder' => 'Manager',
                //'multiple'=>true
                ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Equipe::class,
        ]);
    }
}
