<?php

namespace App\Entity;

use App\Repository\RechercheTicketRepository;
use Doctrine\ORM\Mapping as ORM;


class RechercheTicket
{

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateDebut;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateFin;
    /**
     * @ORM\ManyToOne(targetEntity=DomainTicket::class, inversedBy="tickets")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Domain;

    /**
     * @ORM\ManyToOne(targetEntity=PrioriteTicket::class, inversedBy="tickets")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Priorite;

    /**
     * @ORM\ManyToOne(targetEntity=CategorieTicket::class, inversedBy="tickets")
     * @ORM\JoinColumn(nullable=false)
     */
    private $categorie;
    /**
     * @ORM\ManyToOne(targetEntity=SousDomainTicket::class, inversedBy="tickets")
     * @ORM\JoinColumn(nullable=false)
     */
    private $sous_domain;


    /**
     * @ORM\ManyToOne(targetEntity=Utilisateurs::class, inversedBy="tickets")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Affectation;

    /**
     * @ORM\ManyToOne(targetEntity=StatutTicket::class, inversedBy="tickets")
     * @ORM\JoinColumn(nullable=false)
     */
    private $status;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDateDebut(): ?\DateTimeInterface
    {
        return $this->dateDebut;
    }

    public function setDateDebut(?\DateTimeInterface $dateDebut): self
    {
        $this->dateDebut = $dateDebut;

        return $this;
    }

    public function getDateFin(): ?\DateTimeInterface
    {
        return $this->dateFin;
    }

    public function setDateFin(?\DateTimeInterface $dateFin): self
    {
        $this->dateFin = $dateFin;

        return $this;
    }
    public function getCategorie(): ?CategorieTicket
    {
        return $this->categorie;
    }

    public function setCategorie(?CategorieTicket $categorie): self
    {
        $this->categorie = $categorie;

        return $this;
    }

    public function getDomain(): ?DomainTicket
    {
        return $this->Domain;
    }

    public function setDomain(?DomainTicket $Domain): self
    {
        $this->Domain = $Domain;

        return $this;
    }

    public function getPriorite(): ?PrioriteTicket
    {
        return $this->Priorite;
    }

    public function setPriorite(?PrioriteTicket $Priorite): self
    {
        $this->Priorite = $Priorite;

        return $this;
    }
    public function getSousDomain(): ?SousDomainTicket
    {
        return $this->sous_domain;
    }

    public function setSousDomain(?SousDomainTicket $sous_domain): self
    {
        $this->sous_domain = $sous_domain;

        return $this;
    }

    public function getAffectation (): ?Utilisateurs
    {
        return $this->Affectation;
    }

    public function setAffectation(?Utilisateurs $Affectation): self
    {
        $this->Affectation = $Affectation;

        return $this;
    }
    public function getStatus(): ?StatutTicket
    {
        return $this->status;
    }

    public function setStatus(?StatutTicket $status): self
    {
        $this->status = $status;

        return $this;
    }
}
