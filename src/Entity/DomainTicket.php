<?php

namespace App\Entity;

use App\Repository\DomainTicketRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DomainTicketRepository::class)
 */
class DomainTicket
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom_domain;


    /**
     * @ORM\Column(type="datetime")
     */
    private $date_domain;


    /**
     * @ORM\OneToMany(targetEntity=Ticket::class, mappedBy="Domain", cascade={"persist", "remove"})
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $tickets;

    /**
     * @ORM\OneToMany(targetEntity=SousDomainTicket::class, mappedBy="domain", orphanRemoval=true)
     */
    private $sous_domain;

    public function __toString(): string
    {
        return (string) $this->getNomDomain();
    }

    public function __construct()
    {
        $this->date_domain= new \Datetime();
        $this->tickets = new ArrayCollection();
        $this->sous_domain = new ArrayCollection();
    }

    public function getDateDomain(): ?\DateTimeInterface
    {
        return $this->date_domain;
    }

    public function setDateDomain(\DateTimeInterface $date_domain): self
    {
        $this->date_domain= $date_domain;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomDomain(): ?string
    {
        return $this->nom_domain;
    }

    public function setNomDomain(string $nom_domain): self
    {
        $this->nom_domain = $nom_domain;

        return $this;
    }

    /**
     * @return Collection|Ticket[]
     */
    public function getTickets(): Collection
    {
        return $this->tickets;
    }

    public function addTicket(Ticket $ticket): self
    {
        if (!$this->tickets->contains($ticket)) {
            $this->tickets[] = $ticket;
            $ticket->setDomain($this);
        }

        return $this;
    }

    public function removeTicket(Ticket $ticket): self
    {
        if ($this->tickets->removeElement($ticket)) {
            // set the owning side to null (unless already changed)
            if ($ticket->getDomain() === $this) {
                $ticket->setDomain(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|SousDomainTicket[]
     */
    public function getSousDomain(): Collection
    {
        return $this->sous_domain;
    }

    public function addSousDomain(SousDomainTicket $sousDomain): self
    {
        if (!$this->sous_domain->contains($sousDomain)) {
            $this->sous_domain[] = $sousDomain;
            $sousDomain->setDomain($this);
        }

        return $this;
    }

    public function removeSousDomain(SousDomainTicket $sousDomain): self
    {
        if ($this->sous_domain->removeElement($sousDomain)) {
            // set the owning side to null (unless already changed)
            if ($sousDomain->getDomain() === $this) {
                $sousDomain->setDomain(null);
            }
        }

        return $this;
    }
}
