<?php

namespace App\Entity;

use App\Repository\TicketRepository;
use App\Entity\Messages;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TicketRepository::class)
 * @ORM\Table(name="ticket", indexes={@ORM\Index(columns={"titre_ticket"}, flags={"fulltext"})})
 */
class Ticket
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
  

    /**
     * @ORM\Column(type="datetime")
     */
    private $date_ticket;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $titre_ticket;

   
    /**
     * @ORM\ManyToOne(targetEntity=CategorieTicket::class, inversedBy="tickets")
     * @ORM\JoinColumn(onDelete="CASCADE")
     * @ORM\JoinColumn(nullable=false)
     */
    private $categorie;

    /**
     * @ORM\ManyToOne(targetEntity=DomainTicket::class, inversedBy="tickets")
     * @ORM\JoinColumn(onDelete="CASCADE")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Domain;

    /**
     * @ORM\ManyToOne(targetEntity=PrioriteTicket::class, inversedBy="tickets")
     * @ORM\JoinColumn(onDelete="CASCADE")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Priorite;

    /**
     * @ORM\ManyToOne(targetEntity=SousDomainTicket::class, inversedBy="tickets")
     * @ORM\JoinColumn(onDelete="CASCADE")
     * @ORM\JoinColumn(nullable=false)
     */
    private $sous_domain;

    /**
     * @ORM\ManyToOne(targetEntity=Utilisateurs::class, inversedBy="tickets")
     * @ORM\JoinColumn(onDelete="CASCADE")
     * @ORM\JoinColumn(nullable=false)
     */
    private $utilisateurs;

    /**
     * @ORM\OneToMany(targetEntity=UploadFile::class, mappedBy="messages" , cascade={"persist","remove"})
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $file;



    /**
     * @ORM\ManyToOne(targetEntity=StatutTicket::class, inversedBy="ticket")
     * @ORM\JoinColumn(onDelete="CASCADE")
     * @ORM\JoinColumn(nullable=true)
     */
    private $statut_ticket;

 /**
 * @ORM\ManyToOne(targetEntity=Utilisateurs::class, inversedBy="tickets")
 * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
 */
private $affectation;

    /**
     * @ORM\ManyToOne(targetEntity=Utilisateurs::class, inversedBy="ticketsClient")
     */
    private $Id_client;

    

    /**
     * @ORM\Column(type="text")
     */
    private $commentaire;

    /**
     * @ORM\OneToMany(targetEntity=ReponseSolway::class, mappedBy="ticket")
     */
    private $reponseSolways;

    /**
     * @ORM\OneToMany(targetEntity=ReponseClient::class, mappedBy="ticket")
     */
    private $reponseClients;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $is_closed;

   /**
     * @ORM\OneToMany(targetEntity="Messages", mappedBy="ticket", cascade={"persist", "remove"},orphanRemoval=true)

     */
    private $messages;




    /**
     * @ORM\OneToMany(targetEntity=Etat::class, mappedBy="ticket")
     */
    private $etats;


 

    
   

    public function __construct()
    {
        $this->date_ticket = new \Datetime();
        $this->file = new ArrayCollection();
        $this->affectation_id = new ArrayCollection();
        $this->reponseSolways = new ArrayCollection();
        $this->reponseClients = new ArrayCollection();
        $this->etats = new ArrayCollection();
        $this->messages = new ArrayCollection();

     
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDateTicket(): ?\DateTimeInterface
    {
        return $this->date_ticket;
    }

    public function setDateTicket(\DateTimeInterface $date_ticket): self
    {
        $this->date_ticket = $date_ticket;

        return $this;
    }

    public function getTitreTicket(): ?string
    {
        return $this->titre_ticket;
    }

    public function setTitreTicket(string $titre_ticket): self
    {
        $this->titre_ticket = $titre_ticket;

        return $this;
    }

   

    public function getCategorie(): ?CategorieTicket
    {
        return $this->categorie;
    }

    public function setCategorie(?CategorieTicket $categorie): self
    {
        $this->categorie = $categorie;

        return $this;
    }

    public function getDomain(): ?DomainTicket
    {
        return $this->Domain;
    }

    public function setDomain(?DomainTicket $Domain): self
    {
        $this->Domain = $Domain;

        return $this;
    }

    public function getPriorite(): ?PrioriteTicket
    {
        return $this->Priorite;
    }

    public function setPriorite(?PrioriteTicket $Priorite): self
    {
        $this->Priorite = $Priorite;

        return $this;
    }

    public function getSousDomain(): ?SousDomainTicket
    {
        return $this->sous_domain;
    }

    public function setSousDomain(?SousDomainTicket $sous_domain): self
    {
        $this->sous_domain = $sous_domain;

        return $this;
    }

    public function getUtilisateurs(): ?Utilisateurs
    {
        return $this->utilisateurs;
    }

    public function setUtilisateurs(?Utilisateurs $utilisateurs): self
    {
        $this->utilisateurs = $utilisateurs;

        return $this;
    }

    /**
     * @return Collection|UploadFile[]
     */
    public function getFile(): Collection
    {
        return $this->file;
    }

    public function addFile(UploadFile $file): self
    {
        if (!$this->file->contains($file)) {
            $this->file[] = $file;
            $file->setTicket($this);
        }

        return $this;
    }

    public function removeFile(UploadFile $file): self
    {
        if ($this->file->removeElement($file)) {
            // set the owning side to null (unless already changed)
            if ($file->getTicket() === $this) {
                $file->setTicket(null);
            }
        }

        return $this;
    }


    public function getStatutTicket(): ?StatutTicket
    {
        return $this->statut_ticket;
    }

    public function setStatutTicket(?StatutTicket $statut_ticket): self
    {
        $this->statut_ticket = $statut_ticket;

        return $this;
    }

    public function getAffectation(): ?Utilisateurs
    {
        return $this->affectation;
    }

    public function setAffectation(?Utilisateurs $affectation): self
    {
        $this->affectation = $affectation;

        return $this;
    }

    public function getIdClient(): ?Utilisateurs
    {
        return $this->Id_client;
    }

    public function setIdClient(?Utilisateurs $Id_client): self
    {
        $this->Id_client = $Id_client;

        return $this;
    }


    public function getCommentaire(): ?string
    {
        return $this->commentaire;
    }

    public function setCommentaire(string $commentaire): self
    {
        $this->commentaire = $commentaire;

        return $this;
    }

    /**
     * @return Collection<int, ReponseSolway>
     */
    public function getReponseSolways(): Collection
    {
        return $this->reponseSolways;
    }

    public function addReponseSolway(ReponseSolway $reponseSolway): self
    {
        if (!$this->reponseSolways->contains($reponseSolway)) {
            $this->reponseSolways[] = $reponseSolway;
            $reponseSolway->setTicket($this);
        }

        return $this;
    }

    public function removeReponseSolway(ReponseSolway $reponseSolway): self
    {
        if ($this->reponseSolways->removeElement($reponseSolway)) {
            // set the owning side to null (unless already changed)
            if ($reponseSolway->getTicket() === $this) {
                $reponseSolway->setTicket(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, ReponseClient>
     */
    public function getReponseClients(): Collection
    {
        return $this->reponseClients;
    }

    public function addReponseClient(ReponseClient $reponseClient): self
    {
        if (!$this->reponseClients->contains($reponseClient)) {
            $this->reponseClients[] = $reponseClient;
            $reponseClient->setTicket($this);
        }

        return $this;
    }

    public function removeReponseClient(ReponseClient $reponseClient): self
    {
        if ($this->reponseClients->removeElement($reponseClient)) {
            // set the owning side to null (unless already changed)
            if ($reponseClient->getTicket() === $this) {
                $reponseClient->setTicket(null);
            }
        }

        return $this;
    }

  


/**
     * Get messages
     *
     * @return Collection<int, Messages>
     */
    public function getMessages()
    {
        return $this->messages;
    }



    public function addMessages(Messages $messages): self
    {
        if (!$this->messages->contains($messages)) {
            $this->messages[] = $messages;
            $messages->setTicket($this);
        }

        return $this;
    }

    public function removeMessages(Messages $messages): self
    {
        if ($this->messages->removeElement($messages)) {
            // set the owning side to null (unless already changed)
            if ($messages->getTicket() === $this) {
                $messages->setTicket(null);
            }
        }

        return $this;
    }
/**
     * @ORM\PreRemove
     */
    public function preRemove()
    {
        foreach ($this->messages as $message) {
            $message->setTicket(null);
        }
    }
    public function isIsClosed(): ?bool
    {
        return $this->is_closed;
    }

    public function setIsClosed(?bool $is_closed): self
    {
        $this->is_closed = $is_closed;

        return $this;
    }

    /**
     * @return Collection<int, Etat>
     */
    public function getEtats(): Collection
    {
        return $this->etats;
    }

    public function addEtat(Etat $etat): self
    {
        if (!$this->etats->contains($etat)) {
            $this->etats[] = $etat;
            $etat->setTicket($this);
        }

        return $this;
    }

    public function removeEtat(Etat $etat): self
    {
        if ($this->etats->removeElement($etat)) {
            // set the owning side to null (unless already changed)
            if ($etat->getTicket() === $this) {
                $etat->setTicket(null);
            }
        }

        return $this;
    }

    


   

    
}
