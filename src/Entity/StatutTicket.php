<?php

namespace App\Entity;

use App\Repository\StatutTicketRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=StatutTicketRepository::class)
 */
class StatutTicket
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $etat_ticket;

       /**
     * @ORM\Column(type="datetime")
     */
    private $date_status;


    /**
     * @ORM\OneToMany(targetEntity=Ticket::class, mappedBy="statut_ticket" , cascade={"persist", "remove"})
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $ticket;

    

   
    public function __construct()
    {
        $this->date_status= new \Datetime();
        $this->ticket = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }
    public function getDateStatus(): ?\DateTimeInterface
    {
        return $this->date_status;
    }

    public function setDateStatus(\DateTimeInterface $date_status): self
    {
        $this->date_status= $date_status;

        return $this;
    }

    public function getEtatTicket(): ?string
    {
        return $this->etat_ticket;
    }

    public function setEtatTicket(string $etat_ticket): self
    {
        $this->etat_ticket = $etat_ticket;

        return $this;
    }

    /**
     * @return Collection|Ticket[]
     */
    public function getTicket(): Collection
    {
        return $this->ticket;
    }

    public function addTicket(Ticket $ticket): self
    {
        if (!$this->ticket->contains($ticket)) {
            $this->ticket[] = $ticket;
            $ticket->setStatutTicket($this);
        }

        return $this;
    }

    public function removeTicket(Ticket $ticket): self
    {
        if ($this->ticket->removeElement($ticket)) {
            // set the owning side to null (unless already changed)
            if ($ticket->getStatutTicket() === $this) {
                $ticket->setStatutTicket(null);
            }
        }

        return $this;
    }
}
