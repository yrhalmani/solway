<?php

namespace App\Entity;

use App\Repository\UtilisateursRepository;
use App\Entity\Messages;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * @ORM\Entity(repositoryClass=UtilisateursRepository::class)
 * @UniqueEntity(fields={"email"}, message="cet email existe deja")
 */
class Utilisateurs implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

   /**
 * @var string
 * @ORM\Column(name="email", type="string", length=255)
 * @Assert\Email()
 * exactMessage = "Vous devez renseigner une adresse e-mail valide"
* * 
 */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = ["ROLE_USER"];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\OneToMany(targetEntity=Ticket::class,fetch="EAGER", mappedBy="utilisateurs")
     */
    private $tickets;

    /**
     * @ORM\Column(type="string", length=255)
     * message= "Ce nom est déjà enregistré"
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $activation_token;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $reset_token;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $enabled;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $isVerified;

    /**
     * @ORM\OneToMany(targetEntity=Ticket::class,mappedBy="Id_client")
     */
    private $ticketsClient;

/**
     * @ORM\Column(type="boolean")
     */
    private $isValidByAdmin;

/** 
    * @ORM\Column(type="string",nullable=true)
    * 
   */ 
  private $photo;

  /**
   * @ORM\OneToMany(targetEntity=Equipe::class, mappedBy="manager")
   */
  private $equipesGerer;
   /**
     * @ORM\OneToMany(targetEntity="App\Entity\UploadFile", mappedBy="user")
     */
    private $uploadedFiles;

  /**
   * @ORM\ManyToOne(targetEntity=Equipe::class, inversedBy="membre")
   */
  private $equipe;

  /**
   * @ORM\OneToMany(targetEntity=Messages::class, mappedBy="sender")
   */
  private $sent;

  /**
   * @ORM\OneToMany(targetEntity=Messages::class, mappedBy="recipient")
   */
  private $received;

  /**
   * @ORM\OneToMany(targetEntity=ReponseSolway::class, mappedBy="sender")
   */
  private $reponseSolways;

  /**
   * @ORM\OneToMany(targetEntity=ReponseClient::class, mappedBy="sender")
   */
  private $reponseClients;

 

    public function __construct()
    {
        $this->isVerified = false;
        $this->isValidByAdmin =false; 
        $this->tickets = new ArrayCollection();
        $this->responsables = new ArrayCollection();
        $this->ticketsClient = new ArrayCollection();
        $this->equipes = new ArrayCollection();
        $this->equipesGerer = new ArrayCollection();
        $this->sent = new ArrayCollection();
        $this->received = new ArrayCollection();
        $this->reponseSolways = new ArrayCollection();
        $this->reponseClients = new ArrayCollection();
        $this->uploadedFiles = new ArrayCollection();
     
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }
    public function getPhoto() { 
        return $this->photo; 
     } 
     public function setPhoto($photo) { 
        $this->photo = $photo; 
        return $this; 
     } 

    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * @return Collection|Ticket[]
     */
    public function getTickets(): Collection
    {
        return $this->tickets;
    }

    public function addTicket(Ticket $ticket): self
    {
        if (!$this->tickets->contains($ticket)) {
            $this->tickets[] = $ticket;
            $ticket->setUtilisateurs($this);
        }

        return $this;
    }

    public function removeTicket(Ticket $ticket): self
    {
        if ($this->tickets->removeElement($ticket)) {
            // set the owning side to null (unless already changed)
            if ($ticket->getUtilisateurs() === $this) {
                $ticket->setUtilisateurs(null);
            }
        }

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getActivationToken(): ?string
    {
        return $this->activation_token;
    }
   

    public function setActivationToken(?string $activation_token): self
    {
        $this->activation_token = $activation_token;

        return $this;
    }

    public function getResetToken(): ?string
    {
        return $this->reset_token;
    }

    public function setResetToken(?string $reset_token): self
    {
        $this->reset_token = $reset_token;

        return $this;
    }

    public function getEnabled(): ?bool
    {
        return $this->enabled;
    }

    public function setEnabled(?bool $enabled): self
    {
        $this->enabled = $enabled;

        return $this;
    }

    public function getIsVerified(): ?bool
    {
        return $this->isVerified;
    }

    public function setIsVerified(bool $isVerified): self
    {
        $this->isVerified = $isVerified;

        return $this;
    }

    public function getIsValidByAdmin(): ?bool
    {
        return $this->isValidByAdmin;
    }
    public function setIsValidByAdmin(bool $isValidByAdmin): self
    {
        $this->isValidByAdmin = $isValidByAdmin;
        return $this;
    }


   
    /**
     * @return Collection<int, Ticket>
     */
    public function getTicketsClient(): Collection
    {
        return $this->ticketsClient;
    }

    public function addTicketsClient(Ticket $ticketsClient): self
    {
        if (!$this->ticketsClient->contains($ticketsClient)) {
            $this->ticketsClient[] = $ticketsClient;
            $ticketsClient->setIdClient($this);
        }

        return $this;
    }

    public function removeTicketsClient(Ticket $ticketsClient): self
    {
        if ($this->ticketsClient->removeElement($ticketsClient)) {
            // set the owning side to null (unless already changed)
            if ($ticketsClient->getIdClient() === $this) {
                $ticketsClient->setIdClient(null);
            }
        }

        return $this;
    }

    public function addUploadedFile(UploadFile $uploadedFile): self
    {
        if (!$this->uploadedFiles->contains($uploadedFile)) {
            $this->uploadedFiles[] = $uploadedFile;
            $uploadedFile->setUploadedBy($this);
        }

        return $this;
    }

    public function removeUploadedFile(UploadFile $uploadedFile): self
    {
        if ($this->uploadedFiles->contains($uploadedFile)) {
            $this->uploadedFiles->removeElement($uploadedFile);
            // set the owning side to null (unless already changed)
            if ($uploadedFile->getUploadedBy() === $this) {
                $uploadedFile->setUploadedBy(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Equipe>
     */
    public function getEquipesGerer(): Collection
    {
        return $this->equipesGerer;
    }

    public function addEquipesGerer(Equipe $equipesGerer): self
    {
        if (!$this->equipesGerer->contains($equipesGerer)) {
            $this->equipesGerer[] = $equipesGerer;
            $equipesGerer->setManager($this);
        }

        return $this;
    }

    public function removeEquipesGerer(Equipe $equipesGerer): self
    {
        if ($this->equipesGerer->removeElement($equipesGerer)) {
            // set the owning side to null (unless already changed)
            if ($equipesGerer->getManager() === $this) {
                $equipesGerer->setManager(null);
            }
        }

        return $this;
    }

    public function getEquipe(): ?Equipe
    {
        return $this->equipe;
    }

    public function setEquipe(?Equipe $equipe): self
    {
        $this->equipe = $equipe;

        return $this;
    }

    /**
     * @return Collection<int, Messages>
     */
    public function getSent(): Collection
    {
        return $this->sent;
    }

    public function addSent(Messages $sent): self
    {
        if (!$this->sent->contains($sent)) {
            $this->sent[] = $sent;
            $sent->setSener($this);
        }

        return $this;
    }

    public function removeSent(Messages $sent): self
    {
        if ($this->sent->removeElement($sent)) {
            // set the owning side to null (unless already changed)
            if ($sent->getSener() === $this) {
                $sent->setSener(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Messages>
     */
    public function getReceived(): Collection
    {
        return $this->received;
    }

    public function addReceived(Messages $received): self
    {
        if (!$this->received->contains($received)) {
            $this->received[] = $received;
            $received->setRecipient($this);
        }

        return $this;
    }

    public function removeReceived(Messages $received): self
    {
        if ($this->received->removeElement($received)) {
            // set the owning side to null (unless already changed)
            if ($received->getRecipient() === $this) {
                $received->setRecipient(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, ReponseSolway>
     */
    public function getReponseSolways(): Collection
    {
        return $this->reponseSolways;
    }

    public function addReponseSolway(ReponseSolway $reponseSolway): self
    {
        if (!$this->reponseSolways->contains($reponseSolway)) {
            $this->reponseSolways[] = $reponseSolway;
            $reponseSolway->setSender($this);
        }

        return $this;
    }

    public function removeReponseSolway(ReponseSolway $reponseSolway): self
    {
        if ($this->reponseSolways->removeElement($reponseSolway)) {
            // set the owning side to null (unless already changed)
            if ($reponseSolway->getSender() === $this) {
                $reponseSolway->setSender(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, ReponseClient>
     */
    public function getReponseClients(): Collection
    {
        return $this->reponseClients;
    }

    public function addReponseClient(ReponseClient $reponseClient): self
    {
        if (!$this->reponseClients->contains($reponseClient)) {
            $this->reponseClients[] = $reponseClient;
            $reponseClient->setSender($this);
        }

        return $this;
    }

    public function removeReponseClient(ReponseClient $reponseClient): self
    {
        if ($this->reponseClients->removeElement($reponseClient)) {
            // set the owning side to null (unless already changed)
            if ($reponseClient->getSender() === $this) {
                $reponseClient->setSender(null);
            }
        }

        return $this;
    }

   
  

}

