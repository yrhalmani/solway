<?php

namespace App\Entity;

use App\Repository\ReponseClientRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ReponseClientRepository::class)
 */
class ReponseClient
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Utilisateurs::class, inversedBy="reponseClients")
     */
    private $sender;

    /**
     * @ORM\ManyToOne(targetEntity=Utilisateurs::class, inversedBy="reponseClients")
     */
    private $receiver;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date;

    /**
     * @ORM\ManyToOne(targetEntity=Ticket::class, inversedBy="reponseClients")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $ticket;

    /**
     * @ORM\Column(type="text")
     */
    private $message;


    public function __construct()

    {
        $this->date= new \DateTime();
        //$this->tickets = new ArrayCollection();
    }


    
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSender(): ?Utilisateurs
    {
        return $this->sender;
    }

    public function setSender(?Utilisateurs $sender): self
    {
        $this->sender = $sender;

        return $this;
    }

    public function getReceiver(): ?Utilisateurs
    {
        return $this->receiver;
    }

    public function setReceiver(?Utilisateurs $receiver): self
    {
        $this->receiver = $receiver;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getTicket(): ?Ticket
    {
        return $this->ticket;
    }

    public function setTicket(?Ticket $ticket): self
    {
        $this->ticket = $ticket;

        return $this;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(string $message): self
    {
        $this->message = $message;

        return $this;
    }
}
