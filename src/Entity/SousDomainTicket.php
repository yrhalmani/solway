<?php

namespace App\Entity;

use App\Repository\SousDomainTicketRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SousDomainTicketRepository::class)
 */
class SousDomainTicket
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom_ss_domain;

     /**
     * @ORM\Column(type="datetime")
     */
    private $date_sous_domain;


    /**
     * @ORM\OneToMany(targetEntity=Ticket::class, mappedBy="sous_domain",cascade={"persist", "remove"})
     */
    private $tickets;

    /**
     * @ORM\ManyToOne(targetEntity=DomainTicket::class, inversedBy="sous_domain")
     * @ORM\JoinColumn(nullable=false,)
     */
    private $domain;

    public function __toString()
    {
        return (string) $this->getNomSsDomain();
    }


    public function __construct()
    {
        $this->date_sous_domain= new \Datetime();
        $this->tickets = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDateSsDomain(): ?\DateTimeInterface
    {
        return $this->date_sous_domain;
    }

    public function setDateSsDomain(\DateTimeInterface $date_sous_domain): self
    {
        $this->date_sous_domain= $date_sous_domain;

        return $this;
    }


    public function getNomSsDomain(): ?string
    {
        return $this->nom_ss_domain;
    }

    public function setNomSsDomain(string $nom_ss_domain): self
    {
        $this->nom_ss_domain = $nom_ss_domain;

        return $this;
    }

    /**
     * @return Collection|Ticket[]
     */
    public function getTickets(): Collection
    {
        return $this->tickets;
    }

    public function addTicket(Ticket $ticket): self
    {
        if (!$this->tickets->contains($ticket)) {
            $this->tickets[] = $ticket;
            $ticket->setSousDomain($this);
        }

        return $this;
    }

    public function removeTicket(Ticket $ticket): self
    {
        if ($this->tickets->removeElement($ticket)) {
            // set the owning side to null (unless already changed)
            if ($ticket->getSousDomain() === $this) {
                $ticket->setSousDomain(null);
            }
        }

        return $this;
    }

    public function getDomain(): ?DomainTicket
    {
        return $this->domain;
    }

    public function setDomain(?DomainTicket $domain): self
    {
        $this->domain = $domain;

        return $this;
    }
}
