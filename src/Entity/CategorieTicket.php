<?php

namespace App\Entity;

use App\Repository\CategorieTicketRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CategorieTicketRepository::class)
 */
class CategorieTicket
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom_categorie;

     /**
     * @ORM\Column(type="datetime")
     */
    private $date_category;


    /**
     * @ORM\OneToMany(targetEntity=Ticket::class, mappedBy="categorie",cascade={"persist", "remove"})
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $tickets;

    public function __construct()
    {
        $this->date_category = new \Datetime();
        $this->tickets = new ArrayCollection();
    }

    public function getDateCategorie(): ?\DateTimeInterface
    {
        return $this->date_category;
    }

    public function setDateTicket(\DateTimeInterface $date_category): self
    {
        $this->date_category = $date_category;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomCategorie(): ?string
    {
        return $this->nom_categorie;
    }

    public function setNomCategorie(string $nom_categorie): self
    {
        $this->nom_categorie = $nom_categorie;

        return $this;
    }

    /**
     * @return Collection|Ticket[]
     */
    public function getTickets(): Collection
    {
        return $this->tickets;
    }

    public function addTicket(Ticket $ticket): self
    {
        if (!$this->tickets->contains($ticket)) {
            $this->tickets[] = $ticket;
            $ticket->setCategorie($this);
        }

        return $this;
    }

    public function removeTicket(Ticket $ticket): self
    {
        if ($this->tickets->removeElement($ticket)) {
            // set the owning side to null (unless already changed)
            if ($ticket->getCategorie() === $this) {
                $ticket->setCategorie(null);
            }
        }

        return $this;
    }
}
