<?php

namespace App\Entity;

use App\Repository\PrioriteTicketRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PrioriteTicketRepository::class)
 */
class PrioriteTicket
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $niveau_priorite;
    /**
     * 
     *@ORM\Column(type="datetime")
     */
    private $date_priorite;

    /**
     * @ORM\OneToMany(targetEntity=Ticket::class, mappedBy="Priorite",cascade={"persist", "remove"})
     */
    private $tickets;

    public function __toString()
    {
        return (string) $this->getNiveauPriorite();
    }

    public function __construct()

    {
        $this->date_priorite= new \Datetime();
        $this->tickets = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }
    public function getDatePriorite(): ?\DateTimeInterface
    {
        return $this->date_priorite;
    }

    public function setDatePriorite(\DateTimeInterface $date_priorite): self
    {
        $this->date_priorite= $date_priorite;

        return $this;
    }




    public function getNiveauPriorite(): ?string
    {
        return $this->niveau_priorite;
    }

    public function setNiveauPriorite(string $niveau_priorite): self
    {
        $this->niveau_priorite = $niveau_priorite;

        return $this;
    }

    /**
     * @return Collection|Ticket[]
     */
    public function getTickets(): Collection
    {
        return $this->tickets;
    }

    public function addTicket(Ticket $ticket): self
    {
        if (!$this->tickets->contains($ticket)) {
            $this->tickets[] = $ticket;
            $ticket->setPriorite($this);
        }

        return $this;
    }

    public function removeTicket(Ticket $ticket): self
    {
        if ($this->tickets->removeElement($ticket)) {
            // set the owning side to null (unless already changed)
            if ($ticket->getPriorite() === $this) {
                $ticket->setPriorite(null);
            }
        }

        return $this;
    }
}
