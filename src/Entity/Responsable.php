<?php

namespace App\Entity;

use App\Repository\ResponsableRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use App\Entity\Utilisateurs;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ResponsableRepository::class)
 */
class Responsable
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Nom_responsable;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $prenom_responsable;

    /**
     * @ORM\OneToMany(targetEntity=Responsable::class, mappedBy="responsable",
     * cascade={"persist", "remove"})
     */
    private $ticket;

    /**
     * @ORM\OneToMany(targetEntity=Ticket::class, mappedBy="responsable")
     */
    private $tickets;
    
    /*** @ORM\JoinColumn(name="id", nullable=true)
     * @ORM\OneToOne(targetEntity=Utilisateurs::class,cascade={"persist", "remove","refresh"})
     */
    private
     $id_utilisateur;

   
 /**
     * @ORM\Column(type="json")
     */
    private $roles;


     public function __construct()

    {   
        
        /* $this->Nom_responsable=$user->getName();
        $this->prenom_responsable=$user->getName();
        $this->id_utilisateur=$user;
        */
        


        $this->ticket = new ArrayCollection();
        $this->tickets = new ArrayCollection();
    }

 /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }





    public function getNomResponsable(): ?string
    {
        return $this->Nom_responsable;
    }

    public function setNomResponsable(?string $Nom_responsable): self
    {
        $this->Nom_responsable = $Nom_responsable;

        return $this;
    }

    public function getPrenomResponsable(): ?string
    {
        return $this->prenom_responsable;
    }

    public function setPrenomResponsable(?string $prenom_responsable): self
    {
        $this->prenom_responsable = $prenom_responsable;

        return $this;
    }

    /**
     * @return Collection|Responsable[]
     */
    public function getTicket(): Collection
    {
        return $this->ticket;
    }

    /**
     * @return Collection|Ticket[]
     */
    public function getTickets(): Collection
    {
        return $this->tickets;
    }

    public function getIdUtilisateur(): ?Utilisateurs
    {
        return $this->id_utilisateur;
    }

    public function setIdUtilisateur(?Utilisateurs $id_utilisateur): self
    {
        $this->id_utilisateur = $id_utilisateur;

        return $this;
    }
}

 
