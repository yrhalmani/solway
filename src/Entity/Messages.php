<?php

namespace App\Entity;

use App\Repository\MessagesRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Ticket; 

/**
 * @ORM\Entity(repositoryClass=MessagesRepository::class)
 */
class Messages
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $message;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $is_read;

    /**
     * @ORM\ManyToOne(targetEntity=Utilisateurs::class, inversedBy="sent")
     */
    private $sender;

    /**
     * @ORM\ManyToOne(targetEntity=Utilisateurs::class, inversedBy="received")
     */
    private $recipient;

    /**
     * @ORM\ManyToOne(targetEntity=Ticket::class, inversedBy="messages")
     * @ORM\JoinColumn(nullable=false)
     */
    private $ticket;

    /**
     * @ORM\OneToMany(targetEntity=UploadFile::class, mappedBy="message", cascade={"persist", "remove"})
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $file;

    public function __construct()
    {
        $this->created_at = new \DateTime();
        $this->file = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMessage()
    {
        return $this->message;
    }



    public function setMessage(?string $message): self
    {
        $this->message = $message;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(?\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function isIsRead(): ?bool
    {
        return $this->is_read;
    }

    public function setIsRead(?bool $is_read): self
    {
        $this->is_read = $is_read;

        return $this;
    }

    public function getSender(): ?Utilisateurs
    {
        return $this->sender;
    }

    public function setSender(?Utilisateurs $sender): self
    {
        $this->sender = $sender;

        return $this;
    }

    public function getRecipient(): ?Utilisateurs
    {
        return $this->recipient;
    }

    public function setRecipient(?Utilisateurs $recipient): self
    {
        $this->recipient = $recipient;

        return $this;
    }

    public function getTicket(): ?Ticket
    {
        return $this->ticket;
   }

 
    public function setTicket(?Ticket $ticket): self
    {
        $this->ticket = $ticket;

       return $this;
    }


 /** 
    * public function getFile(): Collection
    *{
     *   return $this->file;
    *}
*/
/** 
    *public function addFile(UploadFile $file): self
    *{
     *   if (!$this->file->contains($file)) {
      *      $this->file[] = $file;
       *     $file->setTicket($this);
        *}

        *return $this;
    *}
*/
 
   /**  
    *  public function removeFile(UploadFile $file): self
    *{
     *   if ($this->file->removeElement($file)) {
      *      // set the owning side to null (unless already changed)
       *     if ($file->getTicket() === $this) {
        *        $file->setTicket(null);
         *   }
        *}

        *return $this;
    *}*/
    public function getFile()
{
    return $this->file;
}

public function addFile(UploadFile $file)
{
    $this->file->add($file);
    $file->setMessage($this);
}
    
}