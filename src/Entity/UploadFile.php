<?php

namespace App\Entity;

use App\Repository\UploadFileRepository;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Messages;


/**
 * @ORM\Entity(repositoryClass=UploadFileRepository::class)
 */
class UploadFile
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_at;


/**
 * @ORM\ManyToOne(targetEntity=Messages::class, inversedBy="file")
 * @ORM\JoinColumn(name="message_id", referencedColumnName="id", nullable=false)
 */
private $message;



    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity=ticket::class, inversedBy="file")
     * @ORM\JoinColumn(nullable=false)
     */
    private $ticket;

    


  /**
 * @ORM\ManyToOne(targetEntity="App\Entity\Utilisateurs", inversedBy="uploadedFiles")
 * @ORM\JoinColumn(nullable=false)
 */
private $user;

    public function __construct()
    {
        $this->created_at = new \DateTime();
    }
     
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getTicket(): ?ticket
    {
        return $this->ticket;
    }

    public function setTicket(?ticket $ticket): self
    {
        $this->ticket = $ticket;

        return $this;
    }
   
    public function getUser(): ?Utilisateurs
    {
        return $this->user;
    }

    public function setUser(?Utilisateurs $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    } 
 
    public function setCreatedAt(?\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }
    public function getMessage(): ?messages
    {
        return $this->messages;
    }

    public function setMessage(?messages $messages): self
    {
        $this->messages = $messages;

        return $this;
    }
 
}