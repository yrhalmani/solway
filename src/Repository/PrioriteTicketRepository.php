<?php

namespace App\Repository;

use App\Entity\PrioriteTicket;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PrioriteTicket|null find($id, $lockMode = null, $lockVersion = null)
 * @method PrioriteTicket|null findOneBy(array $criteria, array $orderBy = null)
 * @method PrioriteTicket[]    findAll()
 * @method PrioriteTicket[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PrioriteTicketRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PrioriteTicket::class);
    }

    // /**
    //  * @return PrioriteTicket[] Returns an array of PrioriteTicket objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PrioriteTicket
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
