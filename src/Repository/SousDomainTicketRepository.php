<?php

namespace App\Repository;

use App\Entity\DomainTicket;
use App\Entity\SousDomainTicket;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\QueryBuilder;

/**
 * @method SousDomainTicket|null find($id, $lockMode = null, $lockVersion = null)
 * @method SousDomainTicket|null findOneBy(array $criteria, array $orderBy = null)
 * @method SousDomainTicket[]    findAll()
 * @method SousDomainTicket[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SousDomainTicketRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SousDomainTicket::class);
    }


    public function findByCountryOrderedByAscName(DomainTicket $country): array
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.nom_domain= :D2')
            ->setParameter('D2', $country)
            ->orderBy('c.nom_ss_domain', 'ASC')
            ->getQuery()
            ->getResult();
    }

    // /**
    //  * @return SousDomainTicket[] Returns an array of SousDomainTicket objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SousDomainTicket
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
