<?php

namespace App\Repository;

use App\Entity\DomainTicket;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method DomainTicket|null find($id, $lockMode = null, $lockVersion = null)
 * @method DomainTicket|null findOneBy(array $criteria, array $orderBy = null)
 * @method DomainTicket[]    findAll()
 * @method DomainTicket[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DomainTicketRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DomainTicket::class);
    }


    public function findAllOrderedByAscNameQueryBuilder(): QueryBuilder
    {
        return $this->createQueryBuilder('c')
            ->orderBy('c.nom_domain', 'ASC');
    }
    // /**
    //  * @return DomainTicket[] Returns an array of DomainTicket objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DomainTicket
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
