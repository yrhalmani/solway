<?php

namespace App\Repository;

use App\Entity\Ticket;
use App\Entity\Utilisateurs;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Ticket|null find($id, $lockMode = null, $lockVersion = null)
 * @method Ticket|null findOneBy(array $criteria, array $orderBy = null)
 * @method Ticket[]    findAll()
 * @method Ticket[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 *  @method  http://127.0.0.1:8000  Ticket[] findFilBytitre($mots)
 */
class TicketRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Ticket::class);
    }

    /**
     * Recherche les tickets en fonction du formulaire (filtrage )
     * @return void
     */
    public function rechercheTicketAdmin(
        $categorie = null,
        $domain = null,
        $sous_domain = null,
        $priorite = null,
        $utilisateur = null
    ) {
        $query = $this->createQueryBuilder('a');
        if ($categorie != null) {
            $query->leftJoin('a.categorie', 'c');
            $query->andWhere('c.id = :id')
                ->setParameter('id', $categorie);
        }
        if ($domain != null) {
            $query->leftJoin('a.Domain', 'd');
            $query->andWhere('d.id = :id2')
                ->setParameter('id2', $domain);
        }
        if ($sous_domain != null) {
            $query->leftJoin('a.sous_domain', 'sd');
            $query->andWhere('sd.id = :id3')
                ->setParameter('id3', $sous_domain);
        }
        if ($priorite != null) {
            $query->leftJoin('a.Priorite', 'p');
            $query->andWhere('p.id = :id4')
                ->setParameter('id4', $priorite);
        }
        if ($utilisateur != null) {
            $query->leftJoin('a.utilisateurs', 'u');
            $query->andWhere('u.id = :id5')
                ->setParameter('id5', $utilisateur);
        }
        return $query->getQuery()->getResult();
    }

    // recherche de ticket par titre

    public function findticket($motcle,$msg)
    {
        $query = $this->createQueryBuilder('f')
            ->where('f.titre_ticket like :titre_ticket ')
            ->setParameter('titre_ticket', '%' . $motcle . '%')
            //->orWhere('f.message like : message')
            //->setParameter('message', '%' . $msg . '%')
            ->orderBy('f.titre_ticket', 'ASC')
            ->getQuery();

        return $query->getResult();
    }

    public function rechercheeTicket($search)

    {

        $query = $this->createQueryBuilder('f')

            ->where('f.titre_ticket LIKE :search')

            //->orWhere('f.message LIKE :search')    
            ->setParameter('search', '%' . $search . '%')
            ->orderBy('f.date_ticket', 'DESC')
            ->getQuery();


            return $query->getResult();

    }


    
    public function findticketbyname($motcle, $rec)
    {
        $query = $this->createQueryBuilder('f')
            ->where('f.titre_ticket like :motcle')
            //->orWhere('f.message LIKE :motcle')
            ->setParameter('motcle', '%' . $motcle . '%')
            ->leftJoin('f.Id_client', 'u')
            ->andWhere('u.id = :id2 ')
            ->setParameter('id2', $rec)
            ->orderBy('f.date_ticket', 'DESC')
            ->getQuery();

        return $query->getResult();
    }
    
    

  
    public function ticketManager($motcle, $rec)
    {
        $query = $this->createQueryBuilder('f')
            ->where('f.titre_ticket like :motcle')
            //->orWhere('f.message LIKE :motcle')
            ->setParameter('motcle', '%' . $motcle . '%')
            ->leftJoin('f.affectation', 'u')
            ->andWhere('u.id = :id2 ')
            ->setParameter('id2', $rec)
            ->orderBy('f.date_ticket', 'DESC')
            ->getQuery();

        return $query->getResult();
    }
    






    public function rechercherFormation(string $search)

    {

        $repo = $this->getDoctrine()->getRepository(Ticket::class);

        $queryBuilder = $repo->createQueryBuilder('f')

            ->where('f.nom LIKE :search')

            ->orWhere('f.contenu LIKE :search')

            ->setParameter('search', '%' . $search . '%')

            ->where('f.active = :status')

            ->setParameter('status', true)

            ->getQuery();


        return $queryBuilder->getResult();

    }




    public function rechercheTicket(

        $categorie = null,
        $domain = null,
        $sous_domain = null,
        $priorite = null

    ) {
        $query = $this->createQueryBuilder('a');

        if ($categorie != null) {
            $query->leftJoin('a.categorie', 'c');
            $query->andWhere('c.id = :id')
                ->setParameter('id', $categorie);
        }
        if ($domain != null) {
            $query->leftJoin('a.Domain', 'd');
            $query->andWhere('d.id = :id2')
                ->setParameter('id2', $domain);
        }
        if ($sous_domain != null) {
            $query->leftJoin('a.sous_domain', 'sd');
            $query->andWhere('sd.id = :id3')
                ->setParameter('id3', $sous_domain);
        }
        if ($priorite != null) {
            $query->leftJoin('a.Priorite', 'p');
            $query->andWhere('p.id = :id4')
                ->setParameter('id4', $priorite);
        }

        return $query->getQuery()->getResult();
    }

    public function findTickets($minValue, $maxValue, $categorie, $domain, $priorite, $sous_domain, 
    $responsable, $user, $status)
    {
        $query = $this->createQueryBuilder('a');
        if ($minValue != null) {
            $query->andWhere('a.date_ticket >= :minVal')
                ->setParameter('minVal', $minValue);
        }
        if ($maxValue != null) {
            $query->andWhere('a.date_ticket <= :maxVal')
                ->setParameter('maxVal', $maxValue);
        }
        if ($categorie != null) {
            $query->leftJoin('a.categorie', 'c');
            $query->andWhere('c.id = :id')
                ->setParameter('id', $categorie);
        }
        if ($domain != null) {
            $query->leftJoin('a.Domain', 'd');
            $query->andWhere('d.id = :id2')
                ->setParameter('id2', $domain);
        }
        if ($priorite != null) {
            $query->leftJoin('a.Priorite', 'p');
            $query->andWhere('p.id = :id4')
                ->setParameter('id4', $priorite);
        }
        if ($sous_domain != null) {
            $query->leftJoin('a.sous_domain', 'sd');
            $query->andWhere('sd.id = :id3')
                ->setParameter('id3', $sous_domain);
        }

        if ($responsable != null) {
            $query->leftJoin('a.affectation', 'idR');
            $query->andWhere('idR.id = :id4')
                ->setParameter('id4', $responsable);
        }
        if ($user != null){
        $query->leftJoin('a.utilisateurs', 'u');
        $query->andWhere('u.id = :idd')
            ->setParameter('idd', $user);
        }
        if ($status!= null) {
            $query->leftJoin('a.statut_ticket', 's');
            $query->andWhere('s.id= :st')
            ->setParameter('st', $status);
        }
        return $query->getQuery()->getResult();
    }















    // /**
    //  * @return Ticket[] Returns an array of Ticket objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    
    public function findOneBySomeField($value): ?Ticket
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.id = :val')
            ->orWhere('t.affectation =: val"')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    
}
