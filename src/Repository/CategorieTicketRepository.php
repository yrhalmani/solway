<?php

namespace App\Repository;

use App\Entity\CategorieTicket;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CategorieTicket|null find($id, $lockMode = null, $lockVersion = null)
 * @method CategorieTicket|null findOneBy(array $criteria, array $orderBy = null)
 * @method CategorieTicket[]    findAll()
 * @method CategorieTicket[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategorieTicketRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CategorieTicket::class);
    }

    // /**
    //  * @return CategorieTicket[] Returns an array of CategorieTicket objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CategorieTicket
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
