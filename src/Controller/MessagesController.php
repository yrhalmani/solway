<?php

namespace App\Controller;

use App\Entity\Messages;
use App\Entity\Ticket;
use App\Entity\Utilisateurs;
use App\Entity\UploadFile ; 
use App\Form\MessagesType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MessagesController extends AbstractController
{
    /**
     * @Route("/messages", name="messages")
     */
    public function index(): Response
    {
        return $this->render('messages/index.html.twig', [
            'controller_name' => 'MessagesController',
        ]);
    }

    /**
     * @Route("/message/{id}", name="message")
     */
    public function message(Request $request,$id): Response
    {
        $reponse = new Messages();
        
    $entityManager = $this->getDoctrine()->getManager();
    $ticket = $entityManager->getRepository(Ticket::class)->find($id);

    // Récupérer la liste des fichiers qui correspondent au ticket en cours
    $ticketFiles = $entityManager->getRepository(UploadFile::class)->findBy(['ticket' => $ticket]);

        $form = $this->createForm(MessagesType::class, $reponse);
        $form->handleRequest($request);

       
    if ($form->isSubmitted() && $form->isValid()) {
        // Traiter le formulaire de téléchargement de fichier
        $files = $form->get('file')->getData();

        foreach ($files as $file) {
            $fileName = md5(uniqid()) . '.' . $file->guessExtension();
            $file->move($this->getParameter('upload_directory'), $fileName);
            $upload = new UploadFile();
            $upload->setName($fileName);
            $upload->setTicket($ticket);
            $user = $this->getUser();
            $upload->setUser($user);
            $entityManager->persist($upload);
            $reponse->addFile($upload);
        }

        $reponse->setSender($this->getUser());
        $reponse->setTicket($ticket);
        $reponse->setRecipient($ticket->getIdClient());
        $entityManager->persist($reponse);
        $entityManager->flush();

            return $this->redirectToRoute('message',['id'=>$id]);
        }
        
        $ticket=$this->getDoctrine()->getRepository(Ticket::class)->find($id);
        return $this->render('conversation/message.html.twig', [
            'reponse' => $reponse,
            'ticket' => $ticket,
            'form' => $form->createView(),
            'ticketFiles' => $ticketFiles,

        ]);
    }

    /**
     * @Route("/received", name="received")
     */
    public function received(): Response
    {
        return $this->render('messages/received.html.twig');
    }

    /**
     * @Route("/sent", name="sent")
     */
    public function sent(): Response
    {
        return $this->render('messages/sent.html.twig');
    }

    /**
     * @Route("/read/{id}", name="read")
     */
    public function read(Messages $messages): Response
    {
        $messages->setIsRead(true);
        $em = $this->getDoctrine()->getManager();
        $em->persist($messages);
        $em->flush();
        return $this->render('messages/read.html.twig', compact("message"));
    }

    /**
     * @Route("/delete/{id}", name="delete")
     */
    public function delete(Messages $messages): Response
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($messages);
        $em->flush();
        return $this->redirectToRoute("received");
    }
}