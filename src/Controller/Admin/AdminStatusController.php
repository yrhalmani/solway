<?php


namespace App\Controller\Admin;

use App\Entity\StatutTicket;
use App\Form\StatusType;
use App\Repository\StatutTicketRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route ("/admin", name="admin_")
 */
class AdminStatusController extends AbstractController
{
    private $repository;
    private $em;

    public function __construct(StatutTicketRepository $repository, EntityManagerInterface $em)
    {
        $this->repository = $repository;
        $this->em = $em;
    }

    /**
     * @Route("/status", name="status")
     */
    public function index(): Response
    {
        return $this->render('admin/status/index.html.twig', [
            "status" => $this->repository->findAll()
        ]);
    }

    /**
     * @Route("/status/new", name="status_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $status = new StatutTicket();
        $form = $this->createForm(StatusType::class, $status);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($status);
            $entityManager->flush();

            return $this->redirectToRoute('admin_status');
        }

        return $this->render('admin/status/new.html.twig', [
            'status' => $status,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("status/edit/{id}", name="status_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, StatutTicket $statut): Response
    {
        $form = $this->createForm(StatusType::class, $statut);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_status');
        }

        return $this->render('admin/status/edit.html.twig', [
            'status' => $statut,
            'form' => $form->createView(),
        ]);
    }

    
    /**
     * @Route("/status/{id}", name="status_delete")
     * @param StatutTicket $status
     * @return RedirectResponse
     */
    public function delete(StatutTicket $status): RedirectResponse
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($status);
        $em->flush();

        return $this->redirectToRoute("admin_status");
    }
}