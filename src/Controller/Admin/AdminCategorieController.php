<?php


namespace App\Controller\Admin;

use App\Entity\CategorieTicket;
use App\Form\CategoryType;
use App\Repository\CategorieTicketRepository;
use App\Repository\PrioriteTicketRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route ("/admin", name="admin_")
 */
class AdminCategorieController extends AbstractController
{
    private $repository;
    private $em;

    public function __construct(CategorieTicketRepository $repository, EntityManagerInterface $em)
    {
        $this->repository = $repository;
        $this->em = $em;
    }

    /**
     * @Route("/category", name="category")
     */
    public function index(): Response
    {
        return $this->render('admin/Categories/index.html.twig', [
            "category" => $this->repository->findAll()
        ]);
    }

    /**
     * @Route("/category/new", name="category_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $category = new CategorieTicket();
        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($category);
            $entityManager->flush();

            return $this->redirectToRoute('admin_category');
        }

        return $this->render('admin/Categories/new.html.twig', [
            'category' => $category,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("category/edit/{id}", name="category_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, CategorieTicket $categorieTicket): Response
    {
        $form = $this->createForm(CategoryType::class, $categorieTicket);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_category');
        }

        return $this->render('admin/Categories/edit.html.twig', [
            'category' => $categorieTicket,
            'form' => $form->createView(),
        ]);
    }

   /**
     * @Route("categorie/{id}", name="categorie_delete")
     * @param CategorieTicket $categorie
     * @return RedirectResponse
     */
    public function delete(CategorieTicket $categorie): RedirectResponse
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($categorie);
        $em->flush();

        return $this->redirectToRoute("admin_category");
    }
}
