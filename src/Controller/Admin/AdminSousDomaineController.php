<?php


namespace App\Controller\Admin;

use App\Entity\SousDomainTicket;
use App\Form\SousDomainesType;
use App\Repository\SousDomainTicketRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route ("/admin", name="admin_")
 */
class AdminSousDomaineController extends AbstractController
{
    private $repository;
    private $em;

    public function __construct(SousDomainTicketRepository $repository, EntityManagerInterface $em)
    {
        $this->repository = $repository;
        $this->em = $em;
    }

    /**
     * @Route("/sous_domaine", name="sous_domaine")
     */
    public function index(): Response
    {
        return $this->render('admin/sous_domaines/index.html.twig', [
            "sousDomaines" => $this->repository->findAll()
        ]);
    }

    /**
     * @Route("/sous_domaine/new", name="sous_domaine_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $sousDomaines = new SousDomainTicket();
        $form = $this->createForm(SousDomainesType::class, $sousDomaines);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($sousDomaines);
            $entityManager->flush();

            return $this->redirectToRoute('admin_sous_domaine');
        }

        return $this->render('admin/sous_domaines/new.html.twig', [
            'sousDomaines' => $sousDomaines,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("sous_domaine/edit/{id}", name="sous_domaine_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, SousDomainTicket $sousDomain): Response
    {
        $form = $this->createForm(SousDomainesType::class, $sousDomain);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_sous_domaine');
        }

        return $this->render('admin/sous_domaines/edit.html.twig', [
            'sousDomaines' => $sousDomain,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("sousDomaine/{id}", name="ssDomaine_delete")
     * @param SousDomaineTicket $ssDomaine
     * @return RedirectResponse
     */
    public function delete(SousDomainTicket $ssDomaine): RedirectResponse
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($ssDomaine);
        $em->flush();

        return $this->redirectToRoute("admin_sous_domaine");
    }
}
