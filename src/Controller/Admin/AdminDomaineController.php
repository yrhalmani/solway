<?php


namespace App\Controller\Admin;

use App\Entity\DomainTicket;
use App\Form\DomaineType;
use App\Repository\DomainTicketRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route ("/admin", name="admin_")
 */
class AdminDomaineController extends AbstractController
{
    private $repository;
    private $em;

    public function __construct(DomainTicketRepository $repository, EntityManagerInterface $em)
    {
        $this->repository = $repository;
        $this->em = $em;
    }

    /**
     * @Route("/domaine", name="domaine")
     */
    public function index(): Response
    {
        return $this->render('admin/domaines/index.html.twig', [
            "domaines" => $this->repository->findAll()
        ]);
    }

    /**
     * @Route("/domaine/new", name="domaine_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $domaines = new DomainTicket();
        $form = $this->createForm(DomaineType::class, $domaines);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($domaines);
            $entityManager->flush();

            return $this->redirectToRoute('admin_domaine');
        }

        return $this->render('admin/domaines/new.html.twig', [
            'domaines' => $domaines,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("domaine/edit/{id}", name="domaine_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, DomainTicket $domain): Response
    {
        $form = $this->createForm(DomaineType::class, $domain);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_domaine');
        }

        return $this->render('admin/domaines/edit.html.twig', [
            'domaines' => $domain,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/domaine/{id}", name="domaine_delete")
     * @param DomaineTicket $domaine
     * @return RedirectResponse
     */
    public function delete(DomainTicket $domaine): RedirectResponse
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($domaine);
        $em->flush();

        return $this->redirectToRoute("admin_domaine");
    }
}
