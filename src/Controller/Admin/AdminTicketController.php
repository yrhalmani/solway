<?php


namespace App\Controller\Admin;

use App\Entity\Etat;
use App\Entity\Messages;
use App\Entity\RechercheTicketAdmin;
use App\Entity\StatutTicket;
use App\Entity\Ticket;
use App\Entity\UploadFile;
use App\Form\AdminTicketType;
use App\Form\AffectTickeType;
use Symfony\Component\Debug\Debug;

use App\Form\EditProfileType;
use App\Form\MessagesType;
use App\Form\TicketType;
use App\Repository\TicketRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Knp\Component\Pager\PaginatorInterface;

use App\Form\RechercheTypeAdmin;
use DateTime;
use Doctrine\Persistence\ManagerRegistry;
use Dompdf\Dompdf;
use Dompdf\Options;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route ("/admin", name="admin_")
 */
class AdminTicketController extends AbstractController
{
    private $repository;
    private $em;


    public function __construct(TicketRepository $repository, EntityManagerInterface $em)
    {
        $this->repository = $repository;
        $this->em = $em;
    }

    /**
     * @Route("/ticket", name="ticket")
     */
    public function index(Request $request, PaginatorInterface $paginator): Response
    {
        $ticket = $this->repository->findAll();
        //$motcle = $request->get('recherche');
        //$ticket = $this->repository->rechercheeTicket($motcle);
        //$ticket = $paginator->paginate(
            //$ticket, /* query NOT result */
            //$request->query->getInt('page', 1)/*page number*/,
            /*limit per page*/
        //);
        return $this->render('admin/ticket/index.html.twig', [
            "ticket" => $ticket,
        ]);
    }


    /**
     * @Route("/ticket/show/{id}", name="ticket_show")
    *   public function show(int $id): Response
    *{

     * return $this->render('admin/ticket/show.html.twig', [
      *      "ticket" => $this->repository->find($id)
       * ]);
    *}
    */






   /**
	     * @Route("/ticket/new", name="ticket_new", methods={"GET","POST"})
	     */
	    public function new(Request $request, MailerInterface $mailer): Response
	    {
	        $ticket = new Ticket();
	        $form = $this->createForm(AdminTicketType::class, $ticket);
	        $form->handleRequest($request);
	
	        if ($form->isSubmitted() && $form->isValid()) {
	            $files = $form->get('file')->getData();
	            foreach ($files as $file) {
	                $fileName = md5(uniqid()) . '.' . $file->guessExtension();
	                $file->move($this->getParameter('upload_directory'), $fileName);
	                $upload = new UploadFile();
	                $upload->setName($fileName);
                    $user = $this->getUser();
        $upload ->setUser($user);
                 $ticket->addFile($upload);
	            }
	            $entityManager = $this->getDoctrine()->getManager();
	            $user = $this->getUser();
	            $ticket->setUtilisateurs($user);

	            $status = $this->getDoctrine()->getRepository(StatutTicket::class)->find('1');
	            $ticket->setStatutTicket($status);
	            $entityManager->persist($ticket);
	            $entityManager->flush();
	            $email = (new TemplatedEmail())
	                ->from('noreplay@solway-cs.com')
	                ->to(new Address($user->getEmail()))
	                ->cc('solway-cs@solway.com')
	                ->bcc('Amine-Hbibiy@solway.cs.com')
	                ->subject('Nouveau ticket sur le site de solway consulting & services')
	                ->text('Sending emails is fun again!')
	                ->htmlTemplate('Email/CréationTicket.html.twig');
	
	            $mailer->send($email);
	            return $this->redirectToRoute('admin_ticket');
	        }
	
	        return $this->render('admin/ticket/new.html.twig', [
	            'ticket' => $ticket,
	            'form' => $form->createView(),
	        ]);
	    }






    /**
     * @Route("ticket/edit/{id}", name="ticket_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Ticket $ticket, MailerInterface $mailer): Response
    {
        $form = $this->createForm(TicketType::class, $ticket, array(
            'is_edit' => true,
        ));
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $user = $this->getUser();
            $status=new Etat();
            $status->setTicket($ticket);
            $status->setStatus($ticket->getStatutTicket());
            $status->setDate(new DateTime('now'));
            $this->getDoctrine()->getManager()->persist($status);
	        //$entityManager->flush();
             //if($ticket->getStatutTicket()->getEtatTicket()=="Terminé")
              //$ticket->setIsClosed(true);
            $this->getDoctrine()->getManager()->flush();
            $email = (new TemplatedEmail())
            ->from('noreplay@solway-cs.com')
            ->to(new Address($user->getEmail()))
            ->cc('solway-cs@solway.com')
            ->bcc('Amine-Hbibiy@solway-cs.com')
            ->subject("Etat d'avancement de votre ticket sur le site de solway consulting
             ")
            ->text('Sending emails is fun again!')
            ->htmlTemplate('Email/statusTicket.html.twig');

            $mailer->send($email);
            return $this->redirectToRoute('admin_message',['id'=>$ticket->getId()]);
           // return $this->render('Email/statusTicket.html.twig', [
                return $this->render('Email/statusTicket.html.twig', [
                    "ticket" => $this->repository->find($ticket->getId())
                   
	        ]);
            
            
        }
           
        

        return $this->render('admin/ticket/edit.html.twig', [
            'ticket' => $ticket,
            'form' => $form->createView(),
        ]);
    }



 /**
     * @Route("ticket/affect/{id}", name="ticket_affectation", methods={"GET","POST"})
     */
    public function affect(Request $request, Ticket $ticket,MailerInterface $mailer ): Response
    {
        $form = $this->createForm(AffectTickeType::class, $ticket);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $user=$ticket->getAffectation();
            $this->getDoctrine()->getManager()->flush();
            $email = (new TemplatedEmail())
            ->from('noreplay@solway-cs.com')
            ->to(new Address($user->getEmail()))
            ->cc('solway-cs@solway.com')
            ->bcc('Amine-Hbibiy@solway-cs.com')
            ->subject('Affectation d\'nouveau ticket')
            ->text('Sending emails is fun again!')
            ->htmlTemplate('Email/Affect.html.twig');

        $mailer->send($email);

            return $this->redirectToRoute('admin_ticket');
        }

        return $this->render('admin/ticket/Affect_ticket.html.twig', [
            'ticket' => $ticket,
            'form' => $form->createView(),
        ]);
    }

/**
     * @Route("/ticket/detail/download/{id}", name="ticket_download")
     */
    public function TicketDownload($id)
    {
        $pdfOptions = new Options();
        $pdfOptions->set('defaultFont', 'Arial');
        $pdfOptions->setIsRemoteEnabled(true);

        $dompdf = new Dompdf($pdfOptions);
        $context = stream_context_create([
            'ssl' => [
                'verify_peer' => FALSE,
                'verify_peer_name' => FALSE,
                'allow_self_signed' => TRUE
            ]
        ]);
        $dompdf->setHttpContext($context);
        // On génère le html
        $html = $this->renderView('admin/ticket/detail_ticket.html.twig', [
            "ticket" => $this->repository->find($id)
        ]);
        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->render();

        // On génère un nom de fichier
        $fichier = 'ticket-' . $this->getUser()->getId() . '.pdf';

        // On envoie le PDF au navigateur
        $dompdf->stream($fichier, [
            'Attachment' => true
        ]);
        return new Response();
    }


    /**
     * @Route("/statistique", name="statistique")
     */

     public function rechercheAvancé(Request $request, PaginatorInterface $paginator): Response
     {
         // $ticket = $this->getUser()->getTickets();
         $ticket = new RechercheTicketAdmin();
         $form = $this->createForm(RechercheTypeAdmin::class, $ticket);
         $form->handleRequest($request);
         $tickets = [];
         if ($form->isSubmitted() && $form->isValid()) {
             $minPrice = $ticket->getDateDebut();
             $maxPrice = $ticket->getDateFin();
             $categorie = $ticket->getCategorie();
             $domain = $ticket->getDomain();
             $sousDomain = $ticket->getSousDomain();
             $priorite = $ticket->getPriorite();
             $user = $ticket->getUtilisateurs();
             $status = $ticket->getStatus();
             $responsable=$ticket->getAffectation();
             $tickets = $this->repository->findTickets(
                 $minPrice,
                 $maxPrice,
                 $categorie,
                 $domain,
                 $priorite,
                 $sousDomain,
                 $responsable,
                 $user,
                 $status,
                
 
             );
         }
         $tickets = $paginator->paginate(
             $tickets, /* query NOT result */
             $request->query->getInt('page', 1)/*page number*/,
             11/*limit per page*/
         );
         return  $this->render('admin/statistiques/statistique.html.twig', [
             'form' => $form->createView(),
             'ticket' => $tickets
         ]);
     }
 

    /**
     * @Route("/statistique/download", name="statistique_download")
     */

    public function statistiqueDownload(Request $request): Response
    {

        $ticket = $this->repository->findAll();
        $form = $this->createForm(RechercheTypeAdmin::class);
        $search = $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            // On recherche les ticket correspondant aux criteres
            $ticket = $this->repository->rechercheTicketAdmin(
                $search->get('categorie')->getData(),
                $search->get('Domain')->getData(),
                $search->get('sous_domain')->getData(),
                $search->get('priorite')->getData(),
                $search->get('utilisateurs')->getData(),
                // $search->get('statut_ticket')->getData(),
            );

            $pdfOptions = new Options();
            $pdfOptions->set('defaultFont', 'Arial');
            $pdfOptions->setIsRemoteEnabled(true);

            $dompdf = new Dompdf($pdfOptions);
            $context = stream_context_create([
                'ssl' => [
                    'verify_peer' => FALSE,
                    'verify_peer_name' => FALSE,
                    'allow_self_signed' => TRUE
                ]
            ]);
            $dompdf->setHttpContext($context);
            // On génère le html
            $html = $this->renderView('admin/ticket/liste_ticket.html.twig', [
                "ticket" => $ticket
            ]);
            $dompdf->loadHtml($html);
            $dompdf->setPaper('A4', 'portrait');
            $dompdf->render();

            // On génère un nom de fichier
            $fichier = 'ticket-' . $this->getUser()->getId() . '.pdf';

            // On envoie le PDF au navigateur
            $dompdf->stream($fichier, [
                'Attachment' => true
            ]);
            return new Response();
        }
        return $this->render('admin/ticket/liste_ticket.html.twig', [
            'ticket' => $ticket,
            'form' => $form->createView()
        ]);
    }
/**
 * @Route("/ticket/{id}", name="ticket_delete")
 * @param Ticket $ticket
 * @return RedirectResponse
 */
public function delete(Ticket $ticket): RedirectResponse
{
    $entityManager = $this->getDoctrine()->getManager();
    $id = $ticket->getId();

    // Récupération du ticket à supprimer
    $ticket = $entityManager->getRepository(Ticket::class)->find($id);

    if (!$ticket) {
        throw $this->createNotFoundException('Le ticket avec l\'id ' . $id . ' n\'existe pas.');
    }

    // Suppression des messages associés au ticket
    foreach ($ticket->getMessages() as $message) {
        $entityManager->remove($message);
    }

    // Suppression du ticket
    $entityManager->remove($ticket);
    $entityManager->flush();

    return $this->redirectToRoute('liste_tickets');
}

     /**
     * @Route("ticket/historique/{id}", name="status_historique", 
     * methods={"GET","POST"})
     */
  public function historique($id): Response
    {
        $ticket = $this->repository->find($id);
       
        return $this->render('admin/ticket/historique.html.twig', [
            "ticket" => $ticket,
        ]);
    }

/**
 * @Route("/solway/{id}", name="message")
 */
public function solway(Request $request, $id): Response
{
    $reponse = new Messages();

    $form = $this->createForm(MessagesType::class, $reponse);

    $entityManager = $this->getDoctrine()->getManager();
    $ticket = $entityManager->getRepository(Ticket::class)->find($id);

    // Récupérer la liste des fichiers qui correspondent au ticket en cours
    $ticketFiles = $entityManager->getRepository(UploadFile::class)->findBy(['ticket' => $ticket]);


    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
        // Traiter le formulaire de téléchargement de fichier
        $files = $form->get('file')->getData();

        foreach ($files as $file) {
            $fileName = md5(uniqid()) . '.' . $file->guessExtension();
            $file->move($this->getParameter('upload_directory'), $fileName);
            $upload = new UploadFile();
            $upload->setName($fileName);
            $upload->setTicket($ticket);
            $user = $this->getUser();
            $upload->setUser($user);
            $entityManager->persist($upload);
            $reponse->addFile($upload);
        }

        $reponse->setSender($this->getUser());
        $reponse->setTicket($ticket);
        $reponse->setRecipient($ticket->getIdClient());
        $entityManager->persist($reponse);
        $entityManager->flush();

        return $this->redirectToRoute('admin_message',['id'=>$id]);
    }

    return $this->render('conversation/messageSolway.html.twig', [
        'reponse' => $reponse,
        'ticket' => $ticket,
        'form' => $form->createView(),
        'ticketFiles' => $ticketFiles
    ]);
}

    






    /**
     *@Route("/profile", name="profile")
     */
    
    public function profile(): Response
    {
        return $this->render('admin/user/profil.html.twig');
    }

   

    
   
    /**
     * @Route("/name/modifier", name="name_edit")
     */
    public function editProfile(Request $request)
    {
        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        $form = $this->createForm(EditProfileType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
         //   $file = $user->getPhoto(); 
           // $fileName = md5(uniqid()).'.'.$file->guessExtension(); 
            //$file->move($this->getParameter('upload_directory'), $fileName); 
            //$user->setPhoto($fileName); 
           // return new Response("User photo is successfully uploaded."); 
         
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            $this->addFlash('message', 'Profile mis à jour !');
            return $this->redirectToRoute('admin_profile');
        }


        return $this->render('admin/user/editProfile.html.twig', [
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/pass/modifier", name="passe_edit")
     */
    public function editPass(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        if ($request->isMethod('POST')) {
            $em = $this->getDoctrine()->getManager();

            $user = $this->getUser();

            // On vérifie si les 2 mots de passe sont identiques
            if ($request->request->get('pass') == $request->request->get('pass2')) {
                $user->setPassword($passwordEncoder->encodePassword($user, $request->request->get('pass')));
                $em->flush();
                $this->addFlash('message', 'Mot de passe mis à jour avec succès');

                return $this->redirectToRoute('admin_profile');
            } else {
                $this->addFlash('error', 'Les deux mots de passe ne sont pas identiques');
            }
        }

        return $this->render('admin/user/EditPasse.html.twig');
    }

   
}