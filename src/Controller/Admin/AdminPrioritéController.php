<?php


namespace App\Controller\Admin;

use App\Entity\PrioriteTicket;
use App\Form\PrioriteType;
use App\Repository\PrioriteTicketRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route ("/admin", name="admin_")
 */
class AdminPrioritéController extends AbstractController
{

    private $repository;
    private $em;

    public function __construct(PrioriteTicketRepository $repository, EntityManagerInterface $em)
    {
        $this->repository = $repository;
        $this->em = $em;
    }

    /**
     * @Route("/priorite", name="priorite")
     */
    public function index(): Response
    {
        return $this->render('admin/priorite/index.html.twig', [
            "priorite" => $this->repository->findAll()
        ]);
    }

    /**
     * @Route("/priorite/new", name="priorite_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $priorite = new PrioriteTicket();
        $form = $this->createForm(PrioriteType::class, $priorite);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($priorite);
            $entityManager->flush();
            
return $this->redirectToRoute('admin_priorite');
        }

        return $this->render('admin/priorite/new.html.twig', [
            'option' => $priorite,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("edit/{id}", name="priorite_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, PrioriteTicket $prioriteTicket): Response
    {
        $form = $this->createForm(PrioriteType::class, $prioriteTicket);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_priorite');
        }

        return $this->render('admin/priorite/edit.html.twig', [
            'option' => $prioriteTicket,
            'form' => $form->createView(),
        ]);
    }

    
     
    /**
     * @Route("/priorite/{id}", name="priorite_delete")
     * @param PrioriteTicket $priorite
     * @return RedirectResponse
     */
    public function delete(PrioriteTicket $priorite): RedirectResponse
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($priorite);
        $em->flush();

        return $this->redirectToRoute("admin_priorite");
    }
}