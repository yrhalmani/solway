<?php


namespace App\Controller\Admin;

use App\Entity\Utilisateurs;
use App\Entity\Responsable;
use App\Entity\UploadFile; 
use App\Entity\Messages; 

use App\Entity\Etat; 
use App\Entity\Ticket; 

use App\Form\UserType;
use App\Entity\ResetPasswordRequest;
use App\Form\UtilisateurType;
use App\Repository\UtilisateursRepository;
use App\Repository\ResponsableRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route ("/admin", name="admin_")
 */
class AdminUserController extends AbstractController
{
    private $repository;
    private $em;

    public function __construct(UtilisateursRepository $repository, EntityManagerInterface $em)
    {
        $this->repository = $repository;
        $this->em = $em;
    }

    /**
     * @Route("/user", name="user")
     */
    public function index(Request $request, PaginatorInterface $paginator): Response
    {
        $user = $this->repository->findBy(array(),array('roles'=>'ASC'));
        
        $user = $paginator->paginate(
            $user, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            5/*limit per page*/
        );
        return $this->render('admin/user/index.html.twig', [
            "user" => $user,
        ]);
    }

/**
 * @Route("/user/new", name="user_new", methods={"GET","POST"})
 */
public function new(Request $request, UserPasswordEncoderInterface $passwordEncoder): Response
{
    $user = new Utilisateurs();
    $form = $this->createForm(UserType::class, $user);
    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
        $user->setPassword(
            $passwordEncoder->encodePassword(
                $user,
                $form->get('password')->getData()
            )
        );

        // Récupérer l'utilisateur connecté qui crée le nouvel utilisateur
        $adminUser = $this->getUser();

        // Vérifier si l'utilisateur connecté est un administrateur
        if ($this->isGranted('ROLE_ADMIN')) {
            // Si l'utilisateur connecté est un administrateur,
            // nous définissons les propriétés sur true
            $user->setIsValidByAdmin(true);
            $user->setIsVerified(true);
        } else {
            // Si l'utilisateur connecté n'est pas un administrateur,
            // nous définissons les propriétés sur false
            $user->setIsValidByAdmin(false);
            $user->setIsVerified(false);
        }

        $user->setActivationToken($this->generateToken());
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($user);
        $entityManager->flush();
        $this->addFlash("notice", "le compte a été crée succes!");
        //return $this->redirectToRoute('admin_user_new');

    }

    return $this->render('admin/user/new.html.twig', [
        'user' => $user,
        'form' => $form->createView(),
    ]);
}

    /**
     * @return string
     * @throws \Exception
     */
    private function generateToken()
    {
        return rtrim(strtr(base64_encode(random_bytes(32)), '+/', '-_'), '=');
    }







  /**
  * @route ("/utilisateur/{id}", name="edit_user")
  */
    public function editRoles(Request $request , 
    Utilisateurs $user,ManagerRegistry $doctrine){
        $form = $this->createForm(UtilisateurType::class, $user, array(
            'is_edit' => true,
        ));
        
      
       
        $form->handleRequest($request);
      
        if ($form->isSubmitted() && $form->isValid()) {
            $doctrine->getManager()->persist($user);
            $doctrine->getManager()->flush();
    
            return $this->redirectToRoute('admin_user');
        
        }
    

        return $this->render('admin/user/EditRoles.html.twig', [
            'ticket' => $user,
            'form' => $form->createView(),
        ]);
    
}


/**
 * @Route("/user/{id}", name="delete_user")
 * @param Utilisateurs $utilisateurs
 * @return RedirectResponse
 */
public function delete(Utilisateurs $utilisateurs): RedirectResponse
{
    $em = $this->getDoctrine()->getManager();

    // Fetch the tickets related to the user
    $tickets = $em->getRepository(Ticket::class)->findBy(['utilisateurs' => $utilisateurs]);

    // Update the affectation_id to null for all related tickets
    $ticketsToDelete = [];
    foreach ($tickets as $ticket) {
        $ticket->setAffectation(null);
        $ticketsToDelete[] = $ticket;
    }

    // Flush the changes to the database
    $em->flush();

    // Remove the etat records related to the tickets
    foreach ($ticketsToDelete as $ticket) {
        $etats = $em->getRepository(Etat::class)->findBy(['ticket' => $ticket]);

        // Remove the etat records
        foreach ($etats as $etat) {
            $em->remove($etat);
        }

        // Remove the ticket
        $em->remove($ticket);
    }

    // Fetch the reset password requests related to the user
    $resetPasswordRequests = $em->getRepository(ResetPasswordRequest::class)->findBy(['user' => $utilisateurs]);

    // Remove the reset password requests
    foreach ($resetPasswordRequests as $resetPasswordRequest) {
        $em->remove($resetPasswordRequest);
    }

    // Fetch the upload files related to the user
    $uploadFiles = $em->getRepository(UploadFile::class)->findBy(['user' => $utilisateurs]);

    // Remove the upload files
    foreach ($uploadFiles as $uploadFile) {
        $em->remove($uploadFile);
    }

  // Fetch the messages related to the user
$messages = $em->getRepository(Messages::class)->findBy(['recipient' => $utilisateurs]);

// Remove the messages if there are any
if ($messages) {
    foreach ($messages as $message) {
        $em->remove($message);
    }
}

// Remove the user
$em->remove($utilisateurs);
$em->flush();

    // Remove the user
    $em->remove($utilisateurs);
    $em->flush();

    return $this->redirectToRoute("admin_user");
}
}