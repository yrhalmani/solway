<?php

namespace App\Controller\Manager;

use App\Entity\Messages;
use App\Entity\RechercheTicket;
use App\Entity\Ticket;
use App\Form\AffectTickeType;
use App\Entity\UploadFile;

use App\Form\MessagesType;
use App\Form\TicketType;

use Symfony\Component\Mime\Address;

use Symfony\Bridge\Twig\Mime\TemplatedEmail;

use App\Form\RechercheTicketsType;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Mailer\MailerInterface;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Repository\TicketRepository;
use Doctrine\ORM\EntityManager;
use App\Form\EditProfileType;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


use Doctrine\ORM\EntityManagerInterface;


/**
 * @Route ("/manager", name="manager_")
 */

class ManagerTeamsController extends AbstractController
{
  
    public function __construct(TicketRepository $repository,EntityManagerInterface $em)
    {
        $this->repository = $repository;
        $this->em = $em;
    }


    /**
     * @Route("/ticket_liste", name="ticket_liste")
     */
    public function index(Request $request, PaginatorInterface $paginator): Response
    {
       
        $motcle = $request->get('recherche');
        $user = $this->getUser();
        $ticket=$this->repository->ticketManager($motcle,$user);
        $ticket = $paginator->paginate(
            $ticket, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            6/*limit per page*/
        );
        return $this->render('Manager/tickets/index.html.twig', [
            "ticket" => $ticket,
        ]);
    }


    /**
     *@Route("/profile", name="profile")
     */
    public function profile(): Response
    {
        return $this->render('Manager/user/profil.html.twig');
    }


    /**
     * @Route("/ticket/show/{id}", name="ticket_show")
     */
    public function show(int $id): Response
    {

        return $this->render('Manager/tickets/show.html.twig', [
            "ticket" => $this->repository->find($id)
        ]);
    }

/**
     * @Route("/recherche_avancé", name="recherche_avancé")
     * Method({"GET"})
     */
    public function rechercheAvancé(Request $request, PaginatorInterface $paginator): Response
    {
        $ticket = new RechercheTicket();
        $form = $this->createForm(RechercheTicketsType::class, $ticket);
        $form->handleRequest($request);
        $tickets = [];
        if ($form->isSubmitted() && $form->isValid()) {
            $minPrice = $ticket->getDateDebut();
            $maxPrice = $ticket->getDateFin();
            $categorie = $ticket->getCategorie();
            $domain = $ticket->getDomain();
            $sousDomain = $ticket->getSousDomain();
            $priorite = $ticket->getPriorite();
            $status=$ticket->getStatus();
            $responsable = $ticket->getAffectation();
            $user = $this->getUser();
            $tickets = $this->repository->findTickets(
                $minPrice,
                $maxPrice,
                $categorie,
                $domain,
                $priorite,
                $sousDomain,
                $responsable,
                $user,
                $status,
            );
        }
        return  $this->render('Manager/statistique/statistique.html.twig', [
            'form' => $form->createView(),
            'ticket' => $tickets
        ]);
    }

   /**
     * @Route("/ticket/creat", name="ticket_new")
     */
    public function ticket_creat(Request $request, MailerInterface $mailer): Response
    {
         $ticket = new Ticket();
        $Form = $this->createForm(TicketType::class, $ticket);
        $Form->handleRequest($request);
        if ($Form->isSubmitted() && $Form->isValid()) {
            $files = $Form->get('file')->getData();
            foreach ($files as $file) {
                $fileName = md5(uniqid()) . '.' . $file->guessExtension();
                $file->move($this->getParameter('upload_directory'));
                $upload = new UploadFile();
                $upload->setName($fileName);
                $user = $this->getUser();
                $upload ->setUser($user);
                $ticket->addFile($upload);
            }
            $em = $this->getDoctrine()->getManager();
            $user = $this->getUser();

            $query = $this->getDoctrine()->getManager()->createQuery('SELECT s FROM App\Entity\StatutTicket s WHERE s.id = :id');
            $query->setParameter('id', 1);
            $status = $query->getOneOrNullResult();

            $ticket->setStatutTicket($status);
            $ticket->setUtilisateurs($user);
            $ticket->setIsClosed(false);

            $ticket->setIdClient($user);
             $repository = $this->getDoctrine()
            ->getManager()
            ->getRepository(Messages::class);

            $message= $repository->FindAll();


            //$status = $this->getDoctrine()->getRepository(StatutTicket::class)->find('1');
            //$ticket->setStatutTicket($status);
           
            /* $message->setIdExpediteur($user);
            $message->setMessage($ticket->getCommentaire());
            $message->setIdTicket($ticket);
            $message->setDateCreation($ticket->getDateTicket());
            $em->persist($message);*/


            $em->persist($ticket);
            $em->flush();
            $email = (new TemplatedEmail())
                ->from('noreplay@solway-cs.com')
                ->to(new Address($user->getEmail()))
                ->cc('solway-cs@solway.com')
                ->bcc('Amine-Hbibiy@solway-cs.com')
                ->subject('Nouveau ticket sur le site de solway consulting & services')
                ->text('Sending emails is fun again!')
                ->htmlTemplate('Email/CréationTicket.html.twig');

            $mailer->send($email);
            return $this->redirectToRoute('ticket_liste', array('id' => $ticket->getId()));
        }


        return $this->render('Manager/tickets/ticketClient_new.html.twig', array(
            'ticket' => $ticket,
            'form' => $Form->createView(),
            
            
/** 'message'=>$message
            */
        ));
    }


















/**
     * @Route("ticket/affect/{id}", name="ticket_affectation", methods={"GET","POST"})
     */
    public function affect(Request $request, Ticket $ticket,MailerInterface $mailer ): Response
    {
        $form = $this->createForm(AffectTickeType::class, $ticket);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $user=$ticket->getAffectation();
            $this->getDoctrine()->getManager()->flush();
            $email = (new TemplatedEmail())
            ->from('noreplay@solway-cs.com')
            ->to(new Address($user->getEmail()))
            ->cc('solway-cs@solway.com')
            ->bcc('Amine-Hbibiy@solway-cs.com')
            ->subject('Affectation d\'nouveau ticket')
            ->text('Sending emails is fun again!')
            ->htmlTemplate('Email/Affect.html.twig');

        $mailer->send($email);

            return $this->redirectToRoute('ticket_liste');
        }

        return $this->render('Manager/tickets/Affect_ticket.html.twig', [
            'ticket' => $ticket,
            'form' => $form->createView(),
        ]);
    }























    /**
 * @Route("/solway/{id}", name="message")
*/
public function solway(Request $request,$id): Response
{
   
    $reponse = new Messages();
        
    $entityManager = $this->getDoctrine()->getManager();
    $ticket = $entityManager->getRepository(Ticket::class)->find($id);

    // Récupérer la liste des fichiers qui correspondent au ticket en cours
    $ticketFiles = $entityManager->getRepository(UploadFile::class)->findBy(['ticket' => $ticket]);

        $form = $this->createForm(MessagesType::class, $reponse);
        $form->handleRequest($request);

       
    if ($form->isSubmitted() && $form->isValid()) {
        // Traiter le formulaire de téléchargement de fichier
        $files = $form->get('file')->getData();

        foreach ($files as $file) {
            $fileName = md5(uniqid()) . '.' . $file->guessExtension();
            $file->move($this->getParameter('upload_directory'), $fileName);
            $upload = new UploadFile();
            $upload->setName($fileName);
            $upload->setTicket($ticket);
            $user = $this->getUser();
            $upload->setUser($user);
            $entityManager->persist($upload);
            $reponse->addFile($upload);
        }

        $reponse->setSender($this->getUser());
        $reponse->setTicket($ticket);
        $reponse->setRecipient($ticket->getIdClient());
        $entityManager->persist($reponse);
        $entityManager->flush();


        return $this->redirectToRoute('manager_message',['id'=>$id]);
    }
    $ticket=$this->getDoctrine()->getRepository(Ticket::class)->find($id);

    return $this->render('conversation/reponseManager.html.twig', [
        'reponse' => $reponse,
        'ticket' => $ticket,
        'form' => $form->createView(),
        'ticketFiles' => $ticketFiles,

        
    ]);

}

    
    /**
     * @Route("/name/modifier", name="name_edit")
     */
    public function editProfile(Request $request)
    {
        $user = $this->getUser();
        $form = $this->createForm(EditProfileType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            $this->addFlash('message', 'Profile mis à jour !');
            return $this->redirectToRoute('manager_profile');
        }

        return $this->render('Manager/user/editProfile.html.twig', [
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/pass/modifier", name="passe_edit")
     */
    public function editPass(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        if ($request->isMethod('POST')) {
            $em = $this->getDoctrine()->getManager();

            $user = $this->getUser();

            // On vérifie si les 2 mots de passe sont identiques
            if ($request->request->get('pass') == $request->request->get('pass2')) {
                $user->setPassword($passwordEncoder->encodePassword($user, $request->request->get('pass')));
                $em->flush();
                $this->addFlash('message', 'Mot de passe mis à jour avec succès');

                return $this->redirectToRoute('manager_profile');         
               } else {
                $this->addFlash('error', 'Les deux mots de passe ne sont pas identiques');
            }
        }

        return $this->render('Manager/user/EditPasse.html.twig');
    }

}


