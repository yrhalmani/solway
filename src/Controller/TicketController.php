<?php

namespace App\Controller;


use App\Entity\RechercheTicket;
use App\Entity\StatutTicket;
use App\Entity\Ticket;
use App\Entity\UploadFile;
use App\Entity\Utilisateurs;
use App\Form\TicketType;
use App\Form\AffectTickeType;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;

use App\Entity\Messages;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\TicketRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Form\EditProfileType;
use App\Form\RechercheTicketsType;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Dompdf\Dompdf;
use Dompdf\Options;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Security\Http\Attribute\CurrentUser;

class TicketController extends AbstractController
{
    public function __construct(TicketRepository $repository, EntityManagerInterface $em)
    {
        $this->repository = $repository;
        $this->em = $em;

    }

    /**
     * @Route("/ticket_liste", name="ticket_liste")
     */
    public function index(Request $request, PaginatorInterface $paginator): Response
    {
       
        $motcle = $request->get('recherche');
        $user = $this->getUser();
        $ticket=$this->repository->findticketbyname($motcle,$user);
        $ticket = $paginator->paginate(
            $ticket, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            6/*limit per page*/
        );
        return $this->render('client/index.html.twig', [
            "ticket" => $ticket,
        ]);
    }

    /**
     * @Route("/ticket/creat", name="ticket_new")
     * @IsGranted("ROLE_USER")
     */
    public function ticket_creat(Request $request, MailerInterface $mailer): Response
    {
         $ticket = new Ticket();
        $Form = $this->createForm(TicketType::class, $ticket);
        $Form->handleRequest($request);
        if ($Form->isSubmitted() && $Form->isValid()) {
            $files = $Form->get('file')->getData();
            foreach ($files as $file) {
                $fileName = md5(uniqid()) . '.' . $file->guessExtension();
                $file->move($this->getParameter('upload_directory'), $fileName);
                $upload = new UploadFile();
                $upload->setName($fileName);
                $user = $this->getUser();
    $upload ->setUser($user);
             $ticket->addFile($upload);
            }
            $em = $this->getDoctrine()->getManager();
            $user = $this->getUser();

            $query = $this->getDoctrine()->getManager()->createQuery('SELECT s FROM App\Entity\StatutTicket s WHERE s.id = :id');
            $query->setParameter('id', 1);
            $status = $query->getOneOrNullResult();

            $ticket->setStatutTicket($status);
            $ticket->setUtilisateurs($user);
            $ticket->setIsClosed(false);

            $ticket->setIdClient($user);
             $repository = $this->getDoctrine()
            ->getManager()
            ->getRepository(Messages::class);

            $message= $repository->FindAll();


            //$status = $this->getDoctrine()->getRepository(StatutTicket::class)->find('1');
            //$ticket->setStatutTicket($status);
           
            /* $message->setIdExpediteur($user);
            $message->setMessage($ticket->getCommentaire());
            $message->setIdTicket($ticket);
            $message->setDateCreation($ticket->getDateTicket());
            $em->persist($message);*/


            $em->persist($ticket);
            $em->flush();
            $email = (new TemplatedEmail())
                ->from('noreplay@solway-cs.com')
                ->to(new Address($user->getEmail()))
                ->cc('solway-cs@solway.com')
                ->bcc('Amine-Hbibiy@solway-cs.com')
                ->subject('Nouveau ticket sur le site de solway consulting & services')
                ->text('Sending emails is fun again!')
                ->htmlTemplate('Email/CréationTicket.html.twig');

            $mailer->send($email);
            return $this->redirectToRoute('ticket_liste', array('id' => $ticket->getId()));
        }


        return $this->render('client/ticketClient_new.html.twig', array(
            'ticket' => $ticket,
            'form' => $Form->createView(),
            
            
/** 'message'=>$message
            */
        ));
    }
    /**
     * @Route("/show_ticket/{id}", name="ticket_show", methods={"GET","POST"})
     */

     public function show($id)
     {
         $ticket = $this->getDoctrine()->getRepository(Ticket::class)->find($id);
         $messages = $ticket->getMessages();
         return $this->render('client/show.html.twig', [
             'ticket' => $ticket,
             'messages' => $messages
         ]);
     } 
 

   /**
 * @Route("/ticket/{id}", name="ticket_delete")
 * @param Ticket $ticket
 * @return RedirectResponse
 */
public function delete(Ticket $ticket): RedirectResponse
{
    $em = $this->getDoctrine()->getManager();

// Supprimer le ticket lui-même
    $em->remove($ticket);
    $em->flush();

    return $this->redirectToRoute("admin_ticket");
}


    /**
     * @Route("/statistique", name="statistique")
     */

    public function statistique(Request $request, PaginatorInterface $paginator): Response
    {
        $ticket = $this->getUser()->getTickets();
       
        $motcle = $request->get('recherche');
        $user = $this->getUser();
        $ticket = $this->repository->findticketbyname($motcle, $user);

        $ticket = $paginator->paginate(
            $ticket, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            3/*limit per page*/
        );
        return $this->render('/statistique/statistique.html.twig', [
            'ticket' => $ticket,

        ]);
    }
 /**
     * @Route("/recherche_avancé", name="recherche_avancé")
     * Method({"GET"})
     */
    public function rechercheAvancé(Request $request, PaginatorInterface $paginator): Response
    {
        $ticket = new RechercheTicket();
        $form = $this->createForm(RechercheTicketsType::class, $ticket);
        $form->handleRequest($request);
        $tickets = [];
        if ($form->isSubmitted() && $form->isValid()) {
            $minPrice = $ticket->getDateDebut();
            $maxPrice = $ticket->getDateFin();
            $categorie = $ticket->getCategorie();
            $domain = $ticket->getDomain();
            $sousDomain = $ticket->getSousDomain();
            $priorite = $ticket->getPriorite();
            $status=$ticket->getStatus();
            $responsable = $ticket->getAffectation();
            $user = $this->getUser();
            $tickets = $this->repository->findTickets(
                $minPrice,
                $maxPrice,
                $categorie,
                $domain,
                $priorite,
                $sousDomain,
                $responsable,
                $user,
                $status
            );
    }

        return  $this->render('statistique/ticketDate.html.twig', [
            'form' => $form->createView(),
            'ticket' => $tickets
        ]);
    }

  /**
    * @Route("ticket/affect/{id}", name="ticket_affectation", methods={"GET","POST"})
    */
   public function affect(Request $request, Ticket $ticket,MailerInterface $mailer ): Response
   {
       $form = $this->createForm(AffectTickeType::class, $ticket);

       $form->handleRequest($request);
       if ($form->isSubmitted() && $form->isValid()) {
           $user=$ticket->getAffectation();
           $this->getDoctrine()->getManager()->flush();
           $email = (new TemplatedEmail())
           ->from('noreplay@solway-cs.com')
           ->to(new Address($user->getEmail()))
           ->cc('solway-cs@solway.com')
           ->bcc('Amine-Hbibiy@solway-cs.com')
           ->subject('Affectation d\'nouveau ticket')
           ->text('Sending emails is fun again!')
           ->htmlTemplate('Email/Affect.html.twig');

       $mailer->send($email);

           return $this->redirectToRoute('ticket_liste');
       }

       return $this->render('client/Affect_ticket.html.twig', [
           'ticket' => $ticket,
           'form' => $form->createView(),
       ]);
   }























     /**
     * @Route("/ticket/detail/download/{id}", name="ticket_download")
     */
    public function TicketDownload($id)
    {

        $pdfOptions = new Options();
        $pdfOptions->set('defaultFont', 'Arial');
        $pdfOptions->setIsRemoteEnabled(true);

        $dompdf = new Dompdf($pdfOptions);
        $context = stream_context_create([
            'ssl' => [
                'verify_peer' => FALSE,
                'verify_peer_name' => FALSE,
                'allow_self_signed' => TRUE
            ]
        ]);
        $dompdf->setHttpContext($context);
        // On génère le html
        $html = $this->renderView('statistique/detail_ticket.html.twig', [
            "ticket" => $this->repository->find($id)
        ]);
        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->render();

        // On génère un nom de fichier
        $fichier = 'ticket-' . $this->getUser()->getId() . '.pdf';

        // On envoie le PDF au navigateur
        $dompdf->stream($fichier, [
            'Attachment' => true
        ]);
        return new Response();
    }










    //---------------------------------------profil utilisateurs-----------------------------


    /**
     * @Route("/profile", name="profile")
     */

    public function profile(): Response
    {
        return $this->render('client/profil.html.twig');
    }


    /**
     * @Route("/profile/name/modifier", name="user_name_edit")
     */
    public function editProfile(Request $request)
    {
        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        $form = $this->createForm(EditProfileType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
          //  $file = $user->getPhoto(); 
            //$fileName = md5(uniqid()).'.'.$file->guessExtension(); 
            //$file->move($this->getParameter('upload_directory'), $fileName); 
            //$user->setPhoto($fileName); 
           // return new Response("User photo is successfully uploaded."); 
         
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            $this->addFlash('message', 'Profile mis à jour !');
            return $this->redirectToRoute('profile');
        }

        return $this->render('client/editProfil.html.twig', [
            'form' => $form->createView(),
        ]);
    }


 /**
     * @Route("/profile/picture/add", name="user_add_picture")
     */




    /**
     * @Route("/profile/pass/modifier", name="user_passe_edit")
     */
    public function editPass(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        if ($request->isMethod('POST')) {
            $em = $this->getDoctrine()->getManager();

            $user = $this->getUser();

            // On vérifie si les 2 mots de passe sont identiques
            if ($request->request->get('pass') == $request->request->get('pass2')) {
                $user->setPassword($passwordEncoder->encodePassword($user, $request->request->get('pass')));
                $em->flush();
                $this->addFlash('message', 'Mot de passe mis à jour avec succès');

                return $this->redirectToRoute('profile');
            } else {
                $this->addFlash('error', 'Les deux mots de passe ne sont pas identiques');
            }
        }

        return $this->render('client/Editpasse.html.twig');
    }
}