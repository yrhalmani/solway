<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230306094320 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
  //   $this->addSql('ALTER TABLE upload_file ADD user_id INT NOT NULL');
        $this->addSql('ALTER TABLE upload_file ADD CONSTRAINT FK_upload_file_user FOREIGN KEY (user_id) REFERENCES utilisateurs(id)');
    }

   
}
