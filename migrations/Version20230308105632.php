<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230308105632 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE upload_file DROP FOREIGN KEY FK_upload_file_utilisateurs');
        $this->addSql('ALTER TABLE upload_file DROP FOREIGN KEY fk_upload_file_user_id');
        $this->addSql('DROP INDEX IDX_81BB169A76ED395 ON upload_file');
        $this->addSql('ALTER TABLE upload_file DROP user_id, CHANGE created_by_id uploaded_by_id INT NOT NULL');
        $this->addSql('ALTER TABLE upload_file ADD CONSTRAINT FK_81BB169A2B28FE8 FOREIGN KEY (uploaded_by_id) REFERENCES utilisateurs (id)');
        $this->addSql('CREATE INDEX IDX_81BB169A2B28FE8 ON upload_file (uploaded_by_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE upload_file DROP FOREIGN KEY FK_81BB169A2B28FE8');
        $this->addSql('DROP INDEX IDX_81BB169A2B28FE8 ON upload_file');
        $this->addSql('ALTER TABLE upload_file ADD user_id INT DEFAULT NULL, CHANGE uploaded_by_id created_by_id INT NOT NULL');
        $this->addSql('ALTER TABLE upload_file ADD CONSTRAINT FK_upload_file_utilisateurs FOREIGN KEY (user_id) REFERENCES utilisateurs (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE upload_file ADD CONSTRAINT fk_upload_file_user_id FOREIGN KEY (user_id) REFERENCES utilisateurs (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_81BB169A76ED395 ON upload_file (user_id)');
    }
}
