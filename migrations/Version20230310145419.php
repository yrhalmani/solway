<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230310145419 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE upload_file ADD CONSTRAINT FK_81BB169A76ED395 FOREIGN KEY (user_id) REFERENCES utilisateurs (id)');
        $this->addSql('CREATE INDEX IDX_81BB169537A1329 ON upload_file (message_id)');
        $this->addSql('CREATE INDEX IDX_81BB169A76ED395 ON upload_file (user_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE upload_file DROP FOREIGN KEY FK_81BB169A76ED395');
        $this->addSql('DROP INDEX IDX_81BB169537A1329 ON upload_file');
        $this->addSql('DROP INDEX IDX_81BB169A76ED395 ON upload_file');
    }
}
