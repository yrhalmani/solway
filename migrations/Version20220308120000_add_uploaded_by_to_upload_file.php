<?php
namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

class Version20220308120000_add_uploaded_by_to_upload_file extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE upload_file ADD uploaded_by_id INT NOT NULL');
        $this->addSql('ALTER TABLE upload_file ADD CONSTRAINT FK_UploadFile_UploadedBy FOREIGN KEY (uploaded_by_id) REFERENCES utilisateurs (id)');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE upload_file DROP FOREIGN KEY FK_UploadFile_UploadedBy');
        $this->addSql('ALTER TABLE upload_file DROP uploaded_by_id');
    }
}
?>