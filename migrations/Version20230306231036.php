<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230306231036 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE upload_file ADD uploaded_by INT NOT NULL');
        $this->addSql('ALTER TABLE upload_file ADD CONSTRAINT FK_uploaded_by FOREIGN KEY (uploaded_by) REFERENCES user (id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE upload_file DROP FOREIGN KEY FK_uploaded_by');
        $this->addSql('ALTER TABLE upload_file DROP uploaded_by');
    }
}
