-- MariaDB dump 10.19  Distrib 10.5.19-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: database.internal    Database: main
-- ------------------------------------------------------
-- Server version	10.4.28-MariaDB-1:10.4.28+maria~deb10

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categorie_ticket`
--

DROP TABLE IF EXISTS `categorie_ticket`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categorie_ticket` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom_categorie` varchar(255) NOT NULL,
  `date_category` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categorie_ticket`
--

LOCK TABLES `categorie_ticket` WRITE;
/*!40000 ALTER TABLE `categorie_ticket` DISABLE KEYS */;
INSERT INTO `categorie_ticket` VALUES (1,'Evolution','2023-02-20 11:34:02'),(2,'Anomalie','2023-02-22 10:31:30');
/*!40000 ALTER TABLE `categorie_ticket` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `doctrine_migration_versions`
--

DROP TABLE IF EXISTS `doctrine_migration_versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `doctrine_migration_versions` (
  `version` varchar(191) NOT NULL,
  `executed_at` datetime DEFAULT NULL,
  `execution_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doctrine_migration_versions`
--

LOCK TABLES `doctrine_migration_versions` WRITE;
/*!40000 ALTER TABLE `doctrine_migration_versions` DISABLE KEYS */;
INSERT INTO `doctrine_migration_versions` VALUES ('DoctrineMigrations\\Version20230113130901','2023-02-20 10:44:47',428655);
/*!40000 ALTER TABLE `doctrine_migration_versions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `domain_ticket`
--

DROP TABLE IF EXISTS `domain_ticket`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `domain_ticket` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom_domain` varchar(255) NOT NULL,
  `date_domain` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `domain_ticket`
--

LOCK TABLES `domain_ticket` WRITE;
/*!40000 ALTER TABLE `domain_ticket` DISABLE KEYS */;
INSERT INTO `domain_ticket` VALUES (1,'TMA','2023-02-20 11:34:12'),(2,'VERIF \'DSN','2023-02-22 10:51:33');
/*!40000 ALTER TABLE `domain_ticket` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `equipe`
--

DROP TABLE IF EXISTS `equipe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `equipe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `manager_id` int(11) DEFAULT NULL,
  `nom_equipe` varchar(255) NOT NULL,
  `date_equipe` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_2449BA15783E3463` (`manager_id`),
  CONSTRAINT `FK_2449BA15783E3463` FOREIGN KEY (`manager_id`) REFERENCES `utilisateurs` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `equipe`
--

LOCK TABLES `equipe` WRITE;
/*!40000 ALTER TABLE `equipe` DISABLE KEYS */;
/*!40000 ALTER TABLE `equipe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `etat`
--

DROP TABLE IF EXISTS `etat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `etat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status_id` int(11) DEFAULT NULL,
  `ticket_id` int(11) DEFAULT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_55CAF7626BF700BD` (`status_id`),
  KEY `IDX_55CAF762700047D2` (`ticket_id`),
  CONSTRAINT `FK_55CAF7626BF700BD` FOREIGN KEY (`status_id`) REFERENCES `statut_ticket` (`id`),
  CONSTRAINT `FK_55CAF762700047D2` FOREIGN KEY (`ticket_id`) REFERENCES `ticket` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `etat`
--

LOCK TABLES `etat` WRITE;
/*!40000 ALTER TABLE `etat` DISABLE KEYS */;
INSERT INTO `etat` VALUES (1,2,9,'2023-02-22 10:54:58'),(2,3,9,'2023-02-22 10:55:13'),(3,4,9,'2023-02-22 10:55:23'),(4,2,9,'2023-02-22 10:55:33'),(5,5,9,'2023-02-22 10:56:11'),(6,2,9,'2023-02-23 15:37:39'),(7,2,15,'2023-02-23 16:39:07'),(8,5,9,'2023-02-23 17:37:33'),(9,2,17,'2023-02-23 17:41:16'),(10,4,26,'2023-02-28 14:25:07'),(11,2,22,'2023-02-28 17:24:38'),(12,4,22,'2023-02-28 17:24:43'),(13,2,22,'2023-02-28 17:24:52'),(14,1,26,'2023-02-28 17:25:37'),(15,3,26,'2023-02-28 17:26:04'),(16,1,72,'2023-03-07 14:41:28'),(17,4,71,'2023-03-07 14:41:56'),(18,3,71,'2023-03-07 14:42:08'),(19,2,113,'2023-03-09 15:29:04'),(20,3,73,'2023-03-09 15:37:33'),(21,4,113,'2023-03-09 15:38:19');
/*!40000 ALTER TABLE `etat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `messages`
--

DROP TABLE IF EXISTS `messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sender_id` int(11) DEFAULT NULL,
  `recipient_id` int(11) DEFAULT NULL,
  `ticket_id` int(11) NOT NULL,
  `message` longtext DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `is_read` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_DB021E96F624B39D` (`sender_id`),
  KEY `IDX_DB021E96E92F8F78` (`recipient_id`),
  KEY `IDX_DB021E96700047D2` (`ticket_id`),
  CONSTRAINT `FK_DB021E96E92F8F78` FOREIGN KEY (`recipient_id`) REFERENCES `utilisateurs` (`id`),
  CONSTRAINT `FK_DB021E96F624B39D` FOREIGN KEY (`sender_id`) REFERENCES `utilisateurs` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=125 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messages`
--

LOCK TABLES `messages` WRITE;
/*!40000 ALTER TABLE `messages` DISABLE KEYS */;
INSERT INTO `messages` VALUES (124,3,NULL,129,'<p>fff</p>','2023-03-15 11:52:15',NULL);
/*!40000 ALTER TABLE `messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `priorite_ticket`
--

DROP TABLE IF EXISTS `priorite_ticket`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `priorite_ticket` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `niveau_priorite` varchar(255) NOT NULL,
  `date_priorite` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `priorite_ticket`
--

LOCK TABLES `priorite_ticket` WRITE;
/*!40000 ALTER TABLE `priorite_ticket` DISABLE KEYS */;
INSERT INTO `priorite_ticket` VALUES (1,'Normal','2023-02-20 11:34:28'),(2,'Urgent','2023-02-22 10:52:15'),(3,'Très urgent','2023-02-22 10:52:28');
/*!40000 ALTER TABLE `priorite_ticket` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reponse_client`
--

DROP TABLE IF EXISTS `reponse_client`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reponse_client` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sender_id` int(11) DEFAULT NULL,
  `receiver_id` int(11) DEFAULT NULL,
  `ticket_id` int(11) DEFAULT NULL,
  `date` datetime NOT NULL,
  `message` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_1310DDCCF624B39D` (`sender_id`),
  KEY `IDX_1310DDCCCD53EDB6` (`receiver_id`),
  KEY `IDX_1310DDCC700047D2` (`ticket_id`),
  CONSTRAINT `FK_1310DDCC700047D2` FOREIGN KEY (`ticket_id`) REFERENCES `ticket` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_1310DDCCCD53EDB6` FOREIGN KEY (`receiver_id`) REFERENCES `utilisateurs` (`id`),
  CONSTRAINT `FK_1310DDCCF624B39D` FOREIGN KEY (`sender_id`) REFERENCES `utilisateurs` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reponse_client`
--

LOCK TABLES `reponse_client` WRITE;
/*!40000 ALTER TABLE `reponse_client` DISABLE KEYS */;
/*!40000 ALTER TABLE `reponse_client` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reponse_solway`
--

DROP TABLE IF EXISTS `reponse_solway`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reponse_solway` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sender_id` int(11) DEFAULT NULL,
  `receiver_id` int(11) DEFAULT NULL,
  `ticket_id` int(11) DEFAULT NULL,
  `date` datetime NOT NULL,
  `message` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_863342D9F624B39D` (`sender_id`),
  KEY `IDX_863342D9CD53EDB6` (`receiver_id`),
  KEY `IDX_863342D9700047D2` (`ticket_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reponse_solway`
--

LOCK TABLES `reponse_solway` WRITE;
/*!40000 ALTER TABLE `reponse_solway` DISABLE KEYS */;
/*!40000 ALTER TABLE `reponse_solway` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reset_password_request`
--

DROP TABLE IF EXISTS `reset_password_request`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reset_password_request` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `selector` varchar(20) NOT NULL,
  `hashed_token` varchar(100) NOT NULL,
  `requested_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `expires_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  PRIMARY KEY (`id`),
  KEY `IDX_7CE748AA76ED395` (`user_id`),
  CONSTRAINT `FK_7CE748AA76ED395` FOREIGN KEY (`user_id`) REFERENCES `utilisateurs` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reset_password_request`
--

LOCK TABLES `reset_password_request` WRITE;
/*!40000 ALTER TABLE `reset_password_request` DISABLE KEYS */;
INSERT INTO `reset_password_request` VALUES (1,3,'RbMfkt7uX17ZnIfCXlNA','DbvZh8ebP+UlUar5Vf6DJU/SCiOcpDKVEDNxyW0hK7A=','2023-02-22 11:27:57','2023-02-22 12:27:57');
/*!40000 ALTER TABLE `reset_password_request` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `responsable`
--

DROP TABLE IF EXISTS `responsable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `responsable` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom_responsable` varchar(255) DEFAULT NULL,
  `prenom_responsable` varchar(255) DEFAULT NULL,
  `roles` longtext NOT NULL COMMENT '(DC2Type:json)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `responsable`
--

LOCK TABLES `responsable` WRITE;
/*!40000 ALTER TABLE `responsable` DISABLE KEYS */;
/*!40000 ALTER TABLE `responsable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sous_domain_ticket`
--

DROP TABLE IF EXISTS `sous_domain_ticket`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sous_domain_ticket` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `domain_id` int(11) NOT NULL,
  `nom_ss_domain` varchar(255) NOT NULL,
  `date_sous_domain` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_C5A4A4E5115F0EE5` (`domain_id`),
  CONSTRAINT `FK_C5A4A4E5115F0EE5` FOREIGN KEY (`domain_id`) REFERENCES `domain_ticket` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sous_domain_ticket`
--

LOCK TABLES `sous_domain_ticket` WRITE;
/*!40000 ALTER TABLE `sous_domain_ticket` DISABLE KEYS */;
INSERT INTO `sous_domain_ticket` VALUES (1,1,'GA','2023-02-20 11:34:21'),(2,2,'TIME','2023-02-22 10:53:19'),(3,1,'Paie','2023-02-22 10:53:29'),(4,2,'Autres','2023-02-22 10:53:40');
/*!40000 ALTER TABLE `sous_domain_ticket` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `statut_ticket`
--

DROP TABLE IF EXISTS `statut_ticket`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `statut_ticket` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `etat_ticket` varchar(255) NOT NULL,
  `date_status` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `statut_ticket`
--

LOCK TABLES `statut_ticket` WRITE;
/*!40000 ALTER TABLE `statut_ticket` DISABLE KEYS */;
INSERT INTO `statut_ticket` VALUES (1,'Nouveau','2023-02-20 11:34:37'),(2,'En cours','2023-02-22 10:54:05'),(3,'À recetter','2023-02-22 10:54:15'),(4,'Réouvert','2023-02-22 10:54:23'),(5,'Terminé','2023-02-22 10:54:32');
/*!40000 ALTER TABLE `statut_ticket` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ticket`
--

DROP TABLE IF EXISTS `ticket`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ticket` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `categorie_id` int(11) DEFAULT NULL,
  `domain_id` int(11) DEFAULT NULL,
  `priorite_id` int(11) DEFAULT NULL,
  `sous_domain_id` int(11) DEFAULT NULL,
  `utilisateurs_id` int(11) DEFAULT NULL,
  `statut_ticket_id` int(11) DEFAULT NULL,
  `affectation_id` int(11) DEFAULT NULL,
  `id_client_id` int(11) DEFAULT NULL,
  `date_ticket` datetime NOT NULL,
  `titre_ticket` varchar(255) NOT NULL,
  `commentaire` longtext NOT NULL,
  `is_closed` tinyint(1) DEFAULT NULL,
  `downloadUrl` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_97A0ADA3BCF5E72D` (`categorie_id`),
  KEY `IDX_97A0ADA3115F0EE5` (`domain_id`),
  KEY `IDX_97A0ADA353B4F1DE` (`priorite_id`),
  KEY `IDX_97A0ADA3A56E5AA` (`sous_domain_id`),
  KEY `IDX_97A0ADA31E969C5` (`utilisateurs_id`),
  KEY `IDX_97A0ADA3D1AED210` (`statut_ticket_id`),
  KEY `IDX_97A0ADA36D0ABA22` (`affectation_id`),
  KEY `IDX_97A0ADA399DED506` (`id_client_id`),
  FULLTEXT KEY `IDX_97A0ADA33D032169` (`titre_ticket`),
  CONSTRAINT `FK_97A0ADA3115F0EE5` FOREIGN KEY (`domain_id`) REFERENCES `domain_ticket` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_97A0ADA31E969C5` FOREIGN KEY (`utilisateurs_id`) REFERENCES `utilisateurs` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_97A0ADA353B4F1DE` FOREIGN KEY (`priorite_id`) REFERENCES `priorite_ticket` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_97A0ADA36D0ABA22` FOREIGN KEY (`affectation_id`) REFERENCES `utilisateurs` (`id`),
  CONSTRAINT `FK_97A0ADA399DED506` FOREIGN KEY (`id_client_id`) REFERENCES `utilisateurs` (`id`),
  CONSTRAINT `FK_97A0ADA3A56E5AA` FOREIGN KEY (`sous_domain_id`) REFERENCES `sous_domain_ticket` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_97A0ADA3BCF5E72D` FOREIGN KEY (`categorie_id`) REFERENCES `categorie_ticket` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_97A0ADA3D1AED210` FOREIGN KEY (`statut_ticket_id`) REFERENCES `statut_ticket` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=130 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ticket`
--

LOCK TABLES `ticket` WRITE;
/*!40000 ALTER TABLE `ticket` DISABLE KEYS */;
INSERT INTO `ticket` VALUES (129,1,1,1,1,3,1,38,NULL,'2023-03-15 11:07:15','l','<p>SS</p>',NULL,NULL);
/*!40000 ALTER TABLE `ticket` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `upload_file`
--

DROP TABLE IF EXISTS `upload_file`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `upload_file` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ticket_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `message_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_81BB169700047D2` (`ticket_id`),
  KEY `FK_upload_file_utilisateurs` (`user_id`),
  KEY `FK_upload_file_messages` (`message_id`),
  CONSTRAINT `FK_81BB169700047D2` FOREIGN KEY (`ticket_id`) REFERENCES `ticket` (`id`),
  CONSTRAINT `FK_upload_file_messages` FOREIGN KEY (`message_id`) REFERENCES `messages` (`id`),
  CONSTRAINT `FK_upload_file_utilisateurs` FOREIGN KEY (`user_id`) REFERENCES `utilisateurs` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `upload_file`
--

LOCK TABLES `upload_file` WRITE;
/*!40000 ALTER TABLE `upload_file` DISABLE KEYS */;
INSERT INTO `upload_file` VALUES (65,96,'1cf98b43a50654bc80c39a08bf1c435c.png','2023-03-10 15:56:48',3,NULL),(66,96,'fc9f7c352513af221bf7eeec8fb30786.html','2023-03-10 15:57:00',3,NULL),(67,96,'ea5bb1712b6b7567d2265666a8590fa3.png','2023-03-10 15:57:15',3,NULL),(68,69,'ee95203b2504c93c7882f6d48173086a.','2023-03-13 20:53:34',3,NULL),(69,69,'1490a9c4c6ebc8753fe3f754512f1c34.txt','2023-03-14 09:09:05',3,NULL),(70,129,'61b1a029e6d629b0fe24256dd20f15ff.html','2023-03-15 11:07:16',3,NULL);
/*!40000 ALTER TABLE `upload_file` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `utilisateurs`
--

DROP TABLE IF EXISTS `utilisateurs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `utilisateurs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `equipe_id` int(11) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `roles` longtext NOT NULL COMMENT '(DC2Type:json)',
  `password` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `activation_token` varchar(255) DEFAULT NULL,
  `reset_token` varchar(255) DEFAULT NULL,
  `enabled` tinyint(1) DEFAULT NULL,
  `is_verified` tinyint(1) NOT NULL,
  `is_valid_by_admin` tinyint(1) NOT NULL,
  `photo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_497B315E6D861B89` (`equipe_id`),
  CONSTRAINT `FK_497B315E6D861B89` FOREIGN KEY (`equipe_id`) REFERENCES `equipe` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `utilisateurs`
--

LOCK TABLES `utilisateurs` WRITE;
/*!40000 ALTER TABLE `utilisateurs` DISABLE KEYS */;
INSERT INTO `utilisateurs` VALUES (3,NULL,'yousra.rhalmani123@gmail.com','[\"ROLE_ADMIN\"]','$2y$13$kCy/N.09VtjP466S2OR6cenf/9rYkhpVbirCw3/2URf.FkoZXUG3u','rhalmani',NULL,NULL,0,1,1,NULL),(4,NULL,'platform12@gmail.com','[]','$2y$13$pR394IVsE./EiIxa1KAItO56zYCBkLZgd.MmJVReu3.1fnkMUdqDq','Youyou',NULL,NULL,0,1,1,NULL),(33,NULL,'amine.hbibiy@solway-cs.com','[\"ROLE_ADMIN\"]','$argon2id$v=19$m=65536,t=4,p=1$91KOn5fJzoDJ4Zt/s7QhjQ$lV4htXnjU4vgJafORk6YmIj1uf+OJqP0KLybIcEQccE','amine Hbibiy','0MEUcDjrfJfNei7LU4EQnCossMXa8n54r8COpi0git4',NULL,NULL,1,1,NULL),(35,NULL,'yrhalmani@gmail.com','[\"ROLE_USER\"]','$2y$13$Lzwt2/LwywqXRrqOWFf2LuJe7PMVdvP91oqp9.tf6YIgZv0nvjMCS','Test',NULL,NULL,1,1,1,NULL),(37,NULL,'nour@gmail.com','[\"ROLE_USER\"]','$2y$13$7N5lwPLj.EDKu3Hw7fP0o..ziqOyfe8/nQxJkmfY8MmAMn9aF4CR2','nur','hjpJMlcVOyH6uAanYuBOnjLg-9RG7pws594FJe4n16w',NULL,NULL,1,1,NULL),(38,NULL,'mery@gmail.com','[\"ROLE_MANAGER\"]','$argon2id$v=19$m=65536,t=4,p=1$AOxbBBMh9iNwfXHnBXCGxw$JC059K/HJwqY90/AuWA+/JkDQsWncspITv1tuzGJfRk','mery','NWBY4GXNPDGhFMtYoJqc8A1WNzKIXJMdIfpR61ATG50',NULL,NULL,1,1,'/tmp/phpOHmMtx'),(39,NULL,'lad8422@gmail.com','[\"ROLE_USER\"]','$2y$13$rHdClRSBGJKtt8YB/Tb.x.mV6sRBO6N40ps91SIPROq6ac6.nhT4a','h','w5sDOpvZB5IbCWLbc95CjRTriinu3kigfei8C47lBa0',NULL,NULL,1,1,NULL),(40,NULL,'yousrarh621@gmail.com','[\"ROLE_USER\"]','$2y$13$KpUdNJh4Cwa56Xatry3OoueuC.YCZCEZ1NgDuZ/0GVbXpNL7/.REq','hiba Rhalmani',NULL,NULL,1,1,1,NULL);
/*!40000 ALTER TABLE `utilisateurs` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-03-15 11:55:29
